import numpy as np
import matplotlib.pylab as plt
import elphy_to_python as ep
import matplotlib as mpl 
from scipy.optimize import curve_fit
from mpl_toolkits.mplot3d import Axes3D

####################################################
####### PARAMETERS OF THE ANALYSIS #################
####################################################
CROSSING = -25 # (mV) for threshold crossing detection
INITIAL_DISCARD = 100e-3 # (in s) we do not count spikes before this time, to discard initial transients
T_BEFORE_SPIKES = 2e-3 # (s) time to discard before the spike (should be adjusted with CROSSING)
T_AFTER_SPIKES = 7e-3 # (s) time to discard after the spike
AUTOCORREL_WINDOW = 50e-3 # (s) max shift time to determine autocorrelation
####################################################


FIGSIZE = (7,5) # global figsize, common to all generated figures
###### ------- SOME GENERAL FUNCTIONS ----------- ######

def find_crossing_time(t, v, crossing=CROSSING):
    spike_indexes = np.where(np.sign(v[:-1]-crossing)<np.sign(v[1:]-crossing))
    return t[spike_indexes]


def determine_firing_rate(spikes, t0, t1, discard=INITIAL_DISCARD):
    """
    we have spiketimes in spike between t0 and t1
    we want to determine the firing rate and its error
    we discard the 20 first ms (for transients in synaptic dynamics)
    
    ===========================================================
    This handles the error ony due to the determination of a rate
    within a finite sampling interval
    ---------------------------------------------------
    
    to have a symmetric error around the estimate, we do the following
    we start by considering a window at t0+discard, then we look for the last
    spike of the trace, this will delimit the end of the considered interval
    we count the spikes within this interval (including the last one)
    so the mean is given by : n/interval
    We could have add a spike just before t0, this would have produced a freq
    n/interval+1./interval
    and we could have stoped just before the last spike, so we would have gotten
    n/interval-1./interval
    this corresponds to an error of 1./interval
    """
    t0+=discard
    spikes = spikes[spikes>t0]
    
    if len(spikes)==0:
        return 0, 1./(t1-t0)
    elif len(spikes)==1:
        interval = np.max([np.abs(t0-spikes[0]),np.abs(t1-spikes[0])])
        return 1./interval, 1./interval
    else:
        t1 = spikes[-1] # time of the last spike
        return len(spikes)/(t1-t0), 1./(t1-t0)

from signanalysis import autocorrel
    
def calc_subthre_prop(v, spikes, t0, dt, discard=INITIAL_DISCARD,\
                      refrac1=T_BEFORE_SPIKES, refrac2=T_AFTER_SPIKES,
                      autocorrel_window=AUTOCORREL_WINDOW, return_trace=False):
    """
    we discard the first 20ms
    then we need to hande the fact that you have spikes !!!
    so we discard a first refrac1 period followed by the refrac2 spike period

    So we take temporal slices defined by this criteria and then we sum their contribution
    associated with their representative weights
    """
    spikes = spikes-t0 # t starts at 0 for simplicity
    spikes = spikes[spikes>discard] # we discard the first xxx ms
    
    ir1, ir2 = int(refrac1/dt), int(refrac2/dt) # refractory steps
    i_start_slice, i_end_slice = [], []
    i_start_slice.append(int(discard/dt)) # we start at discard

    if len(spikes)>0:
        i_spikes = np.array(spikes/dt, dtype='int')
        for i_s in i_spikes:
            # we give 0 length if two spikes overlap
            i_end_slice.append(max([i_s-ir1,i_start_slice[-1]]))
            i_start_slice.append(i_s+ir2)
    # then need to handle the final case
    i_end_slice.append(len(v)-1)

    total_weight, ac_weight = 0,0 # weight for the all the contributions
    muV_sum, sV_sum, Tv_trace_sum = 0, 0, np.zeros(int(autocorrel_window/dt)+1)
    
    for it0, it1 in zip(i_start_slice, i_end_slice):

        if it1>it0:
            weight = (it1-it0)/1000. # weight as step number over 1000.
            muV_sum += weight*v[it0:it1].mean()
            sV_sum += weight*v[it0:it1].std()
            total_weight += weight
        
        if (it1-it0)>int(autocorrel_window/dt): # if bigger than the window
            v_acf, t_shift = autocorrel(v[it0:it1], autocorrel_window, dt)
            Tv_trace_sum += v_acf*weight
            ac_weight += weight

    if total_weight>0:
        muV_sum /= total_weight
        sV_sum /= total_weight

    if ac_weight>0:
        Tv_trace_sum /= ac_weight
        # then fit of a typical time
        exp_f = lambda t, tau: np.exp(-t/tau) # exponential function
        P, pcov = curve_fit(exp_f, t_shift, Tv_trace_sum)
        Tv_typical = P[0]
    else:
        print "not enough subthreshold time"
        Tv_typical = 1e-9
        nn = int(autocorrel_window/dt)
        t_shift, Tv_trace_sum = np.arange(nn)*dt, np.ones(nn)*.5
        
    if return_trace:
        return muV_sum, sV_sum, t_shift, Tv_typical, Tv_trace_sum
    else:
        return muV_sum, sV_sum, Tv_typical

        
def v_time_trace_plot(t, v, times, QUANT, LABEL, exp, time, crossing=CROSSING):
    ### first we plot the full experiment
    fig1 = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.15, right=0.99, hspace=0.05)
    plt.suptitle(exp+' at '+time)
    sp1 = plt.subplot2grid((3,1), (0,0), rowspan=2)
    sp1.plot(t, v, 'b');sp1.plot([t[0],t[-1]],\
            [crossing,crossing], 'g', lw=3, alpha=.5)
    sp1.set_ylabel('$V_m$ (mV)')
    # sp1.set_xticklabels([])
    sp1.set_xlim([t[0], t[-1]])
    
    for tt in times: plt.plot([tt,tt], [-80,0], 'r')
    sp2 = plt.subplot2grid((3,1), (2,0))
    if len(times[::2])>1:
        sp2.bar(times[::2], QUANT[:len(times[::2])],\
            width=times[1::2]-times[::2], color='k',alpha=.4)
        sp2.set_yticks(np.round(np.linspace(round(QUANT.min())-1,\
            round(QUANT.max())+1, 3, endpoint=True)))
    sp2.set_xlabel('time (s)'); sp2.set_ylabel(LABEL)
    sp2.set_xlim([t[0], t[-1]])
    return fig1


def load_data(filename):
    index, I0, Ts, f, Q, muV, sV, muGn, Tv_ratio, Gs, Fout = np.loadtxt(\
                    filename.replace('1.DAT', '_GRID.txt'), unpack=True)
    times  = np.loadtxt(filename.replace('1.DAT', '_t.txt'), unpack=True)

    params = ep.get_metadata(filename) # metadata


    i_max = int(len(times)/2.)
    index, I0, f, Q, Ts, muV, sV, muGn, Tv_ratio, Fout = index[:i_max],\
                  I0[:i_max], f[:i_max], Q[:i_max], Ts[:i_max],\
                  muV[:i_max], sV[:i_max],\
                  muGn[:i_max], Tv_ratio[:i_max], Fout[:i_max]
    times = times[:2*i_max]
    
    # if len(times)%2==1: # means we finish by a rest period !
    #     index, I0, f, Q, Ts, muV, sV, muGn, Tv_ratio, Fout = index[:-1],\
    #               I0[:-1], f[:-1], Q[:-1], Ts[:-1], muV[:-1], sV[:-1],\
    #               muGn[:-1], Tv_ratio[:-1], Fout[:-1]
    #     times = times[:-1]

    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    try:
        t, data = ep.get_signals_continuous(filename)
        dt = t[1]-t[0]
        params['dt'] = dt
        
    except MemoryError:
        t, data = np.arange(0,10, 10), np.ones(10)*-60
        dt = 0.001/np.float(params['f_acq'])
        params['dt'] = dt
        print 'FILE TOO BIG TO BE LOAD IN THE MEMORY !!!!!!!!'

    # there has bee a problem with the saving of Tv_ratio for some files
    # not a big deal we have Ts, so we can get it through:
    if Tv_ratio.all()==0:
        Tv_ratio = 1./muGn+Ts*1000./(float(params['Cm'])*float(params['Rm']))
    
    return params, t, data, times, exp, time,\
           index, I0, Ts, f, Q, muV, sV, muGn, Tv_ratio, Gs, Fout
           
           
def analyze_experiment(params, t, data, times,\
        index, I0, Ts, f, Q, muV, sV, muGn, Tv_ratio, Gs,
        command_channel=1,
        delay_before_Rm_eval=120e-3,
        filename_for_saving=None):

    steps, dt = np.arange(len(index)), t[1]-t[0]
    
    Fout, sFout, Rm_exp, El_exp = 0.*steps, 0.*steps, 0.*steps, 0.*steps
    Tstart, duration = 0.*steps, 0.*steps
    muV_exp, sV_exp, Tv_exp = 0.*steps, 0.*steps, 0.*steps
    ISI_exp = 0.*steps

    for ii in steps:
        # slice with the spikes
        Tstart[ii], duration[ii] = times[2*ii], times[2*ii+1]-times[2*ii]
        i0, i1 = np.argmin(np.abs(t-times[2*ii])), np.argmin(np.abs(t-times[2*ii+1]))
        spikes = find_crossing_time(t[i0:i1], data[0][i0:i1], crossing=CROSSING)
        if len(spikes)>2:
            isi = np.diff(spikes)
            ISI_exp[ii] = isi.std()/isi.mean()
        Fout[ii], sFout[ii] = determine_firing_rate(spikes, t[i0], t[i1])
        muV_exp[ii], sV_exp[ii], Tv_exp[ii] =\
           calc_subthre_prop(data[0][i0:i1], spikes, t[i0], params['dt'])

        # following empty slice with the current step
        # this is a pulse of 500ms starting after 1000ms,\
        # and we discard the first 200 ms
        if ii<len(index)-1:
            ep_index = 2*ii+1
        else:
            ep_index = 2*ii-1
            
        i0, i3 = np.argmin(np.abs(t-times[ep_index]))+2,\
              np.argmin(np.abs(t-times[ep_index+1]))-2 # start and end of pause
        if i3==0:
            i3=-1 # it means there has been only one episode !

        # looking for the characteristics of the pulse
        DI = np.abs(np.diff(data[command_channel][i0:i3])).max() # pA, pulse

        # we find where the negative pulse start !
        i1 = np.where(np.diff(data[command_channel][i0:i3])<-.5*DI)[0]
        if len(i1)>0:
            i1=i1[0]+i0+1
        else:
            i1=i0+100
            print 'problem with the pulse recognition at step', ii
            
        # then we discard the loading time of the membrane
        i1_bis = i1+int(delay_before_Rm_eval/dt)

        # then where the negative pulse stops
        i2 = np.where(np.diff(data[command_channel][i1_bis:i3+2])>.6*DI)[0]
        if len(i2)<1:
            i2 = i3
        else: i2=i2[0]+i1_bis

        # now we can denoise the DI value
        El_exp[ii] = data[0][i0+1:i1-1].mean()
        
        DI = data[command_channel][i0+1:i1-1].mean()-data[command_channel][i1+1:i2-1].mean()
        DV = El_exp[ii]-data[0][i1_bis+1:i2-1].mean()
        Rm_exp[ii] = 1e3*DV/DI # Mohm (1e3*mV/pA)
        
    if filename_for_saving is not None:
        np.save(filename_for_saving,\
            [steps, index, Tstart, duration,\
             I0, Ts, f, Q, muV, sV,\
             muGn, Tv_ratio, Gs,\
             muV_exp, sV_exp, Tv_exp, Fout, sFout, Rm_exp, El_exp, params])

    return Tstart, duration, Fout, sFout, Rm_exp, El_exp,\
                muV_exp, sV_exp, Tv_exp, ISI_exp
                

def recording_prop_plot(params, times, Fout, sFout, Rm_exp, El_exp,\
                muV_exp, sV_exp, Tv_exp, ISI_exp, exp, time):
                
    ### first we plot the full experiment
    fig2 = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.15, left=.15, right=0.99, hspace=0.05)
    plt.suptitle(exp+' at '+time)

    ax = plt.subplot(411)
    plt.errorbar((times[::2]+times[1::2])/2., Fout, sFout, color='k', marker='+', ms=7)
    plt.ylabel('$\\nu_{(Hz)}$')
    plt.yticks([0, np.round(Fout.mean()+.5,1)])

    ax = plt.subplot(412)
    plt.errorbar((times[::2]+times[1::2])/2., ISI_exp, color='k', marker='+', ms=7)
    plt.ylabel('$CV_{ISI}$')
    plt.yticks([0.5, 1.])

    ax = plt.subplot(413)
    plt.errorbar((times[::2]+times[1::2])/2., El_exp, marker='+', ms=7, color='k')
    plt.ylabel('$E_{L (mV)}$')
    plt.plot([times[0], times[-1]],[float(params['El']), float(params['El'])],
             'k--', lw=4, alpha=.3)
    plt.ylim([-80,-50])
    plt.yticks([-75,-65,-55])

                
    ax = plt.subplot(414)
    plt.errorbar((times[::2]+times[1::2])/2., Rm_exp, marker='+', color='k', ms=7)
    plt.plot([times[0], times[-1]],[np.float(params['Rm']), np.float(params['Rm'])],
             'k--', lw=4, alpha=.3)
    plt.ylabel('$R_{m (M \Omega)}$')
    plt.ylim([50,1300])
    plt.yticks([200,600,1000])
    plt.xlabel('time (s)')

    return fig2


import itertools
           
def reformat_vec_to_handle_multiple_seed(index, Ts,
                                         muV, sV, muGn, Tv_ratio,\
                                         Fout, muV_exp, sV_exp, Tv_exp):

        muV2, sV2, muGn2, Tv_ratio2 =  [], [], [], [] # desired
        Fout2, muV_exp2, sV_exp2, Tv_exp2 = [], [], [], [] # means
        s_Fout2, s_muV_exp2, s_sV_exp2, s_Tv_exp2 = [], [], [], [] # variances
        Ts2 = []

        # need to round first, so that the np.where(==) works fine
        
        muV, sV, muGn, Tv_ratio = np.round(muV, 1), np.round(sV, 1),\
          np.round(muGn, 1), np.round(100*Tv_ratio, 1)/100

        for muV1, sV1, muGn1, Tv1 in itertools.product(np.unique(muV),np.unique(sV), np.unique(muGn), np.unique(Tv_ratio)):
                i_repet = np.where((muV==muV1) & (sV==sV1) & (muGn==muGn1)\
                               & (Tv_ratio==Tv1))
                if len(i_repet[0])>0:

                        # desired values
                        muV2.append(muV[i_repet][0])
                        sV2.append(sV[i_repet][0])
                        muGn2.append(muGn[i_repet][0])
                        Tv_ratio2.append(Tv_ratio[i_repet][0])


                        # observed values (MEAN)
                        muV_exp2.append(muV_exp[i_repet].mean())
                        sV_exp2.append(sV_exp[i_repet].mean())
                        Tv_exp2.append(Tv_exp[i_repet].mean())
                        Fout2.append(Fout[i_repet].mean())

                        # observed values (VARIANCE)
                        s_muV_exp2.append(muV_exp[i_repet].std())
                        s_sV_exp2.append(sV_exp[i_repet].std())
                        s_Tv_exp2.append(Tv_exp[i_repet].std())
                        s_Fout2.append(Fout[i_repet].std())

                        #input
                        Ts2.append(Ts[i_repet][0])

        muV, sV, muGn, Tv_ratio =  np.array(muV2), np.array(sV2), np.array(muGn2), np.array(Tv_ratio2)
        Fout, muV_exp, sV_exp, Tv_exp = np.array(Fout2), np.array(muV_exp2), np.array(sV_exp2), np.array(Tv_exp2)
        s_Fout, s_muV_exp, s_sV_exp, s_Tv_exp = np.array(s_Fout2), np.array(s_muV_exp2), np.array(s_sV_exp2), np.array(s_Tv_exp2)
        Ts = np.array(Ts2)
        
        return muV, sV, muGn, Ts, Tv_ratio, Fout, muV_exp, sV_exp,\
           Tv_exp, s_Fout, s_muV_exp, s_sV_exp, s_Tv_exp

           
def extract_all_from_the_data(t, data, times, index, QTPA, q_label):
    """
    when we do the extraction of the quantities, in addition we can
    make a plot of the autocorrelation function and the distribution
    of the membrane potential, and the isi distribution

    QTPA : quantity to plot against
    """
    
    dt = t[1]-t[0]

    mymap = mpl.colors.LinearSegmentedColormap.from_list(\
            'mycolors',['red','blue'])
    Z = [[0,0],[0,0]]

    levels = np.round(QTPA[np.argsort(QTPA)],1)
    if len(levels)<2:
        levels = np.array([levels[0], levels[0]+.1])
    CS3 = plt.contourf(Z, levels, cmap=mymap)


    fig1 = plt.figure(figsize=FIGSIZE) # autocorrelation
    plt.subplots_adjust(bottom=0.15, left=.14, right=0.92, top=0.92)
    f1=plt.subplot(111)
    plt.xlabel('time shift (ms)')
    plt.ylabel('ACF (norm.)')
    cb = plt.colorbar(CS3,use_gridspec=True)
    cb.set_label(q_label)

    fig2 = plt.figure(figsize=FIGSIZE) # membrane potential dstribution
    plt.subplots_adjust(bottom=0.15, left=.14, right=0.92, top=0.92)
    f2=plt.subplot(111)
    v_bins = np.linspace(-70,-30,40)
    plt.xlabel('$V_m (mV)$')
    plt.ylabel('occurence (norm.)')
    cb = plt.colorbar(CS3,use_gridspec=True)
    cb.set_label(q_label)

    fig3 = plt.figure(figsize=FIGSIZE) # Inter-Spike Interval distrib
    plt.subplots_adjust(bottom=0.15, left=.14, right=0.92, top=0.92)
    f3=plt.subplot(111)
    plt.xlabel('$ISI (ms)$')
    plt.ylabel('spk #')
    cb = plt.colorbar(CS3,use_gridspec=True)
    cb.set_label(q_label)

    for ii in np.unique(index):

        i_repet = np.where(index==ii)[0]

        for jj in i_repet:

            i0, i1 = np.argmin(np.abs(t-times[2*jj])),\
                     np.argmin(np.abs(t-times[2*jj+1]))

            if (QTPA.max()-QTPA.min())>0:
                r = (QTPA[jj]-QTPA.min())/(QTPA.max()-QTPA.min())
            else:
                r=0.

            spikes = find_crossing_time(t[i0:i1], data[0][i0:i1], crossing=CROSSING)

            # autocorrelation
            mV, sVm, t_shift, TV, Tv_trace_sum =\
                    calc_subthre_prop(data[0][i0:i1], spikes,\
                               t[i0], dt, return_trace=True)
            f1.plot(1e3*t_shift, Tv_trace_sum, color=mymap(r,1), lw=1)

            # membrane pot distrib
            vhist=np.histogram(data[0][i0:i1], bins=v_bins, density=True)[0]
            f2.plot(.5*(v_bins[:-1]+v_bins[1:]),vhist, color=mymap(r,1))

            # ISI distrib
            if len(spikes)>2:
                isi = 1e3*np.diff(spikes) # in ms
                isi_bins = np.linspace(isi.min(), isi.max(),5)
                isi_hist=np.histogram(isi, bins=isi_bins)[0]
                f3.bar(.5*(isi_bins[:-1]+isi_bins[1:]),isi_hist,\
                       width = isi_bins[0],\
                       color=mymap(r,1), alpha=.3)
            

    return fig1, fig2, fig3

###############################################################
###### ------- ANALYSIS PROTOCOL BY PROTOCOL ----------- ######
###############################################################

######### total conductance effect

def muGn_effect(filename, crossing=CROSSING, command_channel=2):

    params, t, data, times, exp, time, index, I0, Ts, f, Q,\
       muV, sV, muGn, Tv_ratio, Gs, Fout = load_data(filename)

    fig1 = v_time_trace_plot(t, data[0], times, muGn, '$\mu_G$/$g_L$',\
        '      Varying $\mu_G/g_L$ ',\
        time, crossing=CROSSING)

    try:
        print 'number of steps :', len(index)
    except TypeError:
        return [fig1]
    
    Tstart, duration, Fout, sFout, Rm_exp, El_exp,\
        muV_exp, sV_exp, Tv_exp, ISI_exp =\
         analyze_experiment(params, t, data, times,\
         index, I0, Ts, f, Q, muV, sV, muGn, Tv_ratio, Gs,
         filename_for_saving=time+'_muGn.npy',\
         command_channel=command_channel)

    fig2 = recording_prop_plot(params, times, Fout, sFout, Rm_exp, El_exp,\
                muV_exp, sV_exp, Tv_exp, ISI_exp,\
        '      Varying $\mu_G/g_L$',\
        time)

    fig5, fig6, fig7 = extract_all_from_the_data(t, data, times, index, muGn,\
            '$\mu_G/g_L$')
            
    muV, sV, muGn, Ts, Tv_ratio, Fout, muV_exp, sV_exp,\
           Tv_exp, s_Fout, s_muV_exp, s_sV_exp, s_Tv_exp = \
        reformat_vec_to_handle_multiple_seed(index, Ts, muV, sV, muGn, Tv_ratio,\
                                         Fout, muV_exp, sV_exp, Tv_exp)


    # # then special quantities
    i_muGn = np.argsort(muGn)
    
    fig3 = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.14, left=.11, right=0.99)
    plt.suptitle('      Varying $\mu_G/g_L$ at '+time)
    plt.errorbar(muGn[i_muGn], Fout[i_muGn], s_Fout[i_muGn],\
                 color='k', marker='D')
    plt.ylabel('$\\nu_{out}$ (Hz)');
    plt.xlabel('$\mu_G$ / $g_L$')
    plt.annotate('$\mu_V$= '+str(round(np.float(params['clamp_muV']),1))\
                 +'mV',(.2,.4), xycoords='figure fraction', fontsize=13)
    plt.annotate('$\sigma_V$= '+str(round(np.float(params['clamp_sV']),1))\
                 +'mV',(.2,.32), xycoords='figure fraction', fontsize=13)
    plt.annotate('$\\tau_S / \\tau_m^0$= '+\
                 str(round(100*np.float(params['clamp_Ts_ratio']),1))\
                 +'%',(.2,.25), xycoords='figure fraction', fontsize=13)
    plt.annotate('seed ='+str(np.int(params['seed'])),\
                 (.2,.19), xycoords='figure fraction', fontsize=13)

    # time constant ratio calculus

    Tv_ratio_exp = Tv_exp/(np.float(params['Cm'])*np.float(params['Rm'])*1e-6)
    s_Tv_ratio_exp = s_Tv_exp/(np.float(params['Cm'])*np.float(params['Rm'])*1e-6)
    Ts_ratio = Ts/(np.float(params['Cm'])*np.float(params['Rm'])*1e-3)

    # # then subthreshold properties
    fig4 = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.14, left=.11, right=0.99, hspace=.05)
    plt.subplot(311)
    plt.plot(muGn[i_muGn],np.float(params['clamp_muV'])*np.ones(len(muGn)),\
             'k-', lw=8,alpha=.3)
    plt.errorbar(muGn[i_muGn], muV_exp[i_muGn], s_muV_exp[i_muGn],\
                 color='k', marker='D')
    plt.ylabel('$\mu_V$ (mV)')
    plt.yticks(np.float(params['clamp_muV'])+np.arange(3)*2-2)
    plt.subplot(312)
    plt.plot(muGn[i_muGn],np.float(params['clamp_sV'])*np.ones(len(muGn)),\
             'k-', lw=8,alpha=.3)
    plt.errorbar(muGn[i_muGn], sV_exp[i_muGn], s_sV_exp[i_muGn],\
                 color='k', marker='D')
    plt.yticks(np.float(params['clamp_sV'])+np.arange(3)*2-2)
    plt.ylabel('$\sigma_V$ (mV)')
    plt.subplot(313)
    plt.plot(muGn[i_muGn], 100.*Ts_ratio[i_muGn],\
             'k-', lw=3,alpha=.8, label='$\\tau_S / \\tau_m^0$')
    plt.plot(muGn[i_muGn],100.*Tv_ratio[i_muGn],'k-', lw=3,alpha=.3, label='$\\tau_V / \\tau_m^0$')
    plt.plot(muGn[i_muGn],100./muGn[i_muGn],'k--', lw=2,alpha=.9, label='$\\tau_m / \\tau_m^0$')
    plt.errorbar(muGn[i_muGn], 100.*Tv_ratio_exp[i_muGn], 100.*s_Tv_ratio_exp[i_muGn],\
                 color='k', marker='D')
    plt.legend(frameon=False, prop={'size':'x-small'}, loc='upper right')
    plt.yticks([0, 50, 100])
    plt.ylabel('$\\tau / \\tau_m^0$ (%)')
    plt.xlabel('$\mu_G$ / $g_L$')

    return [fig1, fig2, fig3, fig4, fig5, fig6, fig7]



######### autocorrelation time effect

def Tv_effect(filename, crossing=CROSSING, command_channel=2):

    params, t, data, times, exp, time, index, I0, Ts, f, Q,\
       muV, sV, muGn, Tv_ratio, Gs, Fout = load_data(filename)

    fig1 = v_time_trace_plot(t, data[0], times, 100*Tv_ratio, '$\\tau_V / \\tau_m^0$ (%)',\
        '      Varying $\\tau_V / \\tau_m^0$ ',\
        time, crossing=CROSSING)

    try:
        print 'number of steps :', len(index)
    except TypeError:
        return [fig1]
    
    Tstart, duration, Fout, sFout, Rm_exp, El_exp,\
        muV_exp, sV_exp, Tv_exp, ISI_exp =\
         analyze_experiment(params, t, data, times,\
         index, I0, Ts, f, Q, muV, sV, muGn, Tv_ratio, Gs,
         filename_for_saving=time+'_Tv.npy',\
         command_channel=command_channel)

    fig2 = recording_prop_plot(params, times, Fout, sFout, Rm_exp, El_exp,\
                muV_exp, sV_exp, Tv_exp, ISI_exp,\
        '      Varying $\\tau_V / \\tau_m^0$',\
        time)

    fig5, fig6, fig7 = extract_all_from_the_data(t, data, times, index, 100*Tv_ratio,\
            '$\\tau_V / \\tau_m^0$ (%)')
            
    muV, sV, muGn, Ts, Tv_ratio, Fout, muV_exp, sV_exp,\
           Tv_exp, s_Fout, s_muV_exp, s_sV_exp, s_Tv_exp = \
        reformat_vec_to_handle_multiple_seed(index, Ts, muV, sV, muGn, Tv_ratio,\
                                         Fout, muV_exp, sV_exp, Tv_exp)


    # # then special quantities
    i_Tv_ratio = np.argsort(Tv_ratio)
    
    fig3 = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.14, left=.11, right=0.99)
    plt.suptitle('      Varying $\\tau_V / \\tau_m^0$ at '+time)
    plt.errorbar(100*Tv_ratio[i_Tv_ratio], Fout[i_Tv_ratio], s_Fout[i_Tv_ratio],\
                 color='k', marker='D')
    plt.ylabel('$\\nu_{out}$ (Hz)');
    plt.xlabel('$\\tau / \\tau_m^0$ (%)')
    plt.annotate('$\mu_V$= '+str(round(np.float(params['clamp_muV']),1))\
                 +'mV',(.2,.4), xycoords='figure fraction', fontsize=13)
    plt.annotate('$\sigma_V$= '+str(round(np.float(params['clamp_sV']),1))\
                 +'mV',(.2,.32), xycoords='figure fraction', fontsize=13)
    plt.annotate('$\\tau_S / \\tau_m^0$= '+\
                 str(round(100*np.float(params['clamp_Ts_ratio']),1))\
                 +'%',(.2,.25), xycoords='figure fraction', fontsize=13)
    plt.annotate('seed ='+str(np.int(params['seed'])),\
                 (.2,.19), xycoords='figure fraction', fontsize=13)

    # time constant ratio calculus

    Tv_ratio_exp = Tv_exp/(np.float(params['Cm'])*np.float(params['Rm'])*1e-6)
    s_Tv_ratio_exp = s_Tv_exp/(np.float(params['Cm'])*np.float(params['Rm'])*1e-6)

    # # then subthreshold properties
    fig4 = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.14, left=.11, right=0.99, hspace=.05)
    plt.subplot(311)
    plt.plot(100*Tv_ratio[i_Tv_ratio],np.float(params['clamp_muV'])*np.ones(len(Tv_ratio)),\
             'k-', lw=8,alpha=.3)
    plt.errorbar(100*Tv_ratio[i_Tv_ratio], muV_exp[i_Tv_ratio], s_muV_exp[i_Tv_ratio],\
                 color='k', marker='D')
    plt.ylabel('$\mu_V$ (mV)')
    plt.yticks(np.float(params['clamp_muV'])+np.arange(3)*2-2)
    plt.subplot(312)
    plt.plot(100*Tv_ratio[i_Tv_ratio],np.float(params['clamp_sV'])*np.ones(len(100*Tv_ratio)),\
             'k-', lw=8,alpha=.3)
    plt.errorbar(100*Tv_ratio[i_Tv_ratio], sV_exp[i_Tv_ratio], s_sV_exp[i_Tv_ratio],\
                 color='k', marker='D')
    plt.yticks(np.float(params['clamp_sV'])+np.arange(3)*2-2)
    plt.ylabel('$\sigma_V$ (mV)')
    plt.subplot(313)
    plt.plot([100*Tv_ratio.min(), 100*Tv_ratio.max()],\
             [100.*float(params['clamp_Ts_ratio']),100.*float(params['clamp_Ts_ratio'])],\
             'k-', lw=3,alpha=.8, label='$\\tau_S / \\tau_m^0$')
    plt.plot(100*Tv_ratio[i_Tv_ratio],100.*Tv_ratio[i_Tv_ratio],'k-', lw=3,alpha=.3, label='$\\tau_V / \\tau_m^0$')
    plt.plot(100*Tv_ratio[i_Tv_ratio],100./muGn[i_Tv_ratio],'k--', lw=2,alpha=.9, label='$\\tau_m / \\tau_m^0$')
    plt.errorbar(100*Tv_ratio[i_Tv_ratio], 100.*Tv_ratio_exp[i_Tv_ratio], 100.*s_Tv_ratio_exp[i_Tv_ratio],\
                 color='k', marker='D')
    plt.legend(frameon=False, prop={'size':'x-small'}, loc='upper right')
    plt.yticks([0, 50, 100])
    plt.ylabel('$\\tau / \\tau_m^0$ (%)')
    plt.xlabel('$\\tau / \\tau_m^0$ (%)')

    return [fig1, fig2, fig3, fig4, fig5, fig6, fig7]


######### total conductance and autocorrelation effect

def muGn_and_Tv_effect(filename, crossing=CROSSING,\
         command_channel=2):

    params, t, data, times, exp, time, index, I0, Ts, f, Q,\
       muV, sV, muGn, Tv_ratio, Gs, Fout = load_data(filename)

    fig1 = v_time_trace_plot(t, data[0], times, muGn, '$\mu_G$/$g_L$',\
        '      Varying $\mu_G/g_L = (\\tau_V/\\tau_m^0-\\tau_S/\\tau_m^0)^{-1}$',\
        time, crossing=CROSSING)

    try:
        print 'number of steps :', len(index)
    except TypeError:
        return [fig1]
    
    Tstart, duration, Fout, sFout, Rm_exp, El_exp,\
        muV_exp, sV_exp, Tv_exp, ISI_exp =\
         analyze_experiment(params, t, data, times,\
         index, I0, Ts, f, Q, muV, sV, muGn, Tv_ratio, Gs,
         filename_for_saving=time+'_muGn_Tv.npy',\
         command_channel=command_channel)

    fig2 = recording_prop_plot(params, times, Fout, sFout, Rm_exp, El_exp,\
                muV_exp, sV_exp, Tv_exp, ISI_exp,\
        '      Varying $\mu_G/g_L = (\\tau_V/\\tau_m^0-\\tau_S/\\tau_m^0)^{-1}$',\
        time)

    fig5, fig6, fig7 = extract_all_from_the_data(t, data, times, index, muGn,\
            '$\mu_G/g_L = (\\tau_V/\\tau_m^0-\\tau_S/\\tau_m^0)^{-1}$')
            
    muV, sV, muGn, Ts, Tv_ratio, Fout, muV_exp, sV_exp,\
           Tv_exp, s_Fout, s_muV_exp, s_sV_exp, s_Tv_exp = \
        reformat_vec_to_handle_multiple_seed(index, Ts, muV, sV, muGn, Tv_ratio,\
                                         Fout, muV_exp, sV_exp, Tv_exp)


    # # then special quantities
    i_muGn = np.argsort(muGn)
    
    fig3 = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.14, left=.11, right=0.99)
    plt.suptitle('      Varying $\mu_G/g_L = (\\tau_V/\\tau_m^0-\\tau_S/\\tau_m^0)^{-1}$ at '+time)
    plt.errorbar(muGn[i_muGn], Fout[i_muGn], s_Fout[i_muGn],\
                 color='k', marker='D')
    plt.ylabel('$\\nu_{out}$ (Hz)');
    plt.xlabel('$\mu_G$ / $g_L$')
    plt.annotate('$\mu_V$= '+str(round(np.float(params['clamp_muV']),1))\
                 +'mV',(.2,.4), xycoords='figure fraction', fontsize=13)
    plt.annotate('$\sigma_V$= '+str(round(np.float(params['clamp_sV']),1))\
                 +'mV',(.2,.32), xycoords='figure fraction', fontsize=13)
    plt.annotate('$\\tau_S / \\tau_m^0$= '+\
                 str(round(100*np.float(params['clamp_Ts_ratio']),1))\
                 +'%',(.2,.25), xycoords='figure fraction', fontsize=13)
    plt.annotate('seed ='+str(np.int(params['seed'])),\
                 (.2,.19), xycoords='figure fraction', fontsize=13)

    # time constant ratio calculus

    Tv_ratio_exp = Tv_exp/(np.float(params['Cm'])*np.float(params['Rm'])*1e-6)
    s_Tv_ratio_exp = s_Tv_exp/(np.float(params['Cm'])*np.float(params['Rm'])*1e-6)

    # # then subthreshold properties
    fig4 = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.14, left=.11, right=0.99, hspace=.05)
    plt.subplot(311)
    plt.plot(muGn[i_muGn],np.float(params['clamp_muV'])*np.ones(len(muGn)),\
             'k-', lw=8,alpha=.3)
    plt.errorbar(muGn[i_muGn], muV_exp[i_muGn], s_muV_exp[i_muGn],\
                 color='k', marker='D')
    plt.ylabel('$\mu_V$ (mV)')
    plt.yticks(np.float(params['clamp_muV'])+np.arange(3)*2-2)
    plt.subplot(312)
    plt.plot(muGn[i_muGn],np.float(params['clamp_sV'])*np.ones(len(muGn)),\
             'k-', lw=8,alpha=.3)
    plt.errorbar(muGn[i_muGn], sV_exp[i_muGn], s_sV_exp[i_muGn],\
                 color='k', marker='D')
    plt.yticks(np.float(params['clamp_sV'])+np.arange(3)*2-2)
    plt.ylabel('$\sigma_V$ (mV)')
    plt.subplot(313)
    plt.plot([muGn.min(), muGn.max()],\
             [100.*float(params['clamp_Ts_ratio']),100.*float(params['clamp_Ts_ratio'])],\
             'k-', lw=3,alpha=.8, label='$\\tau_S / \\tau_m^0$')
    plt.plot(muGn[i_muGn],100.*Tv_ratio[i_muGn],'k-', lw=3,alpha=.3, label='$\\tau_V / \\tau_m^0$')
    plt.plot(muGn[i_muGn],100./muGn[i_muGn],'k--', lw=2,alpha=.9, label='$\\tau_m / \\tau_m^0$')
    plt.errorbar(muGn[i_muGn], 100.*Tv_ratio_exp[i_muGn], 100.*s_Tv_ratio_exp[i_muGn],\
                 color='k', marker='D')
    plt.legend(frameon=False, prop={'size':'x-small'}, loc='upper right')
    plt.yticks([0, 50, 100])
    plt.ylabel('$\\tau / \\tau_m^0$ (%)')
    plt.xlabel('$\mu_G$ / $g_L$')

    return [fig1, fig2, fig3, fig4, fig5, fig6, fig7]


def muV_effect(filename, crossing=CROSSING,\
         command_channel=2):

    params, t, data, times, exp, time, index, I0, Ts, f, Q,\
       muV, sV, muGn, Tv_ratio, Gs, Fout = load_data(filename)

    fig1 = v_time_trace_plot(t, data[0], times, muGn, '$\mu_V$ (mV)',\
        '      Varying $\mu_V$ ',\
        time, crossing=CROSSING)

    try:
        print 'number of steps :', len(index)
    except TypeError:
        return [fig1]
    
    Tstart, duration, Fout, sFout, Rm_exp, El_exp,\
        muV_exp, sV_exp, Tv_exp, ISI_exp =\
         analyze_experiment(params, t, data, times,\
         index, I0, Ts, f, Q, muV, sV, muGn, Tv_ratio, Gs,
         filename_for_saving=time+'_muV.npy',\
         command_channel=command_channel)

    fig2 = recording_prop_plot(params, times, Fout, sFout, Rm_exp, El_exp,\
                muV_exp, sV_exp, Tv_exp, ISI_exp,\
        '      Varying $\mu_V$',\
        time)

    fig5, fig6, fig7 = extract_all_from_the_data(t, data, times, index, muV,\
            '$\mu_V$')
            
    muV, sV, muGn, Ts, Tv_ratio, Fout, muV_exp, sV_exp,\
           Tv_exp, s_Fout, s_muV_exp, s_sV_exp, s_Tv_exp = \
        reformat_vec_to_handle_multiple_seed(index, Ts, muV, sV, muGn, Tv_ratio,\
                                         Fout, muV_exp, sV_exp, Tv_exp)


    # # then special quantities
    i_muV = np.argsort(muV)
    
    fig3 = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.14, left=.11, right=0.99)
    plt.suptitle('      Varying $\mu_V$ at '+time)
    plt.errorbar(muV[i_muV], Fout[i_muV], s_Fout[i_muV],\
                 color='k', marker='D')
    plt.ylabel('$\\nu_{out}$ (Hz)');
    plt.xlabel('$\mu_V$ (mV)')
    plt.annotate('$\mu_G$= '+str(round(np.float(params['clamp_muGn']),1))\
                 +' ',(.2,.4), xycoords='figure fraction', fontsize=13)
    plt.annotate('$\sigma_V$= '+str(round(np.float(params['clamp_sV']),1))\
                 +'mV',(.2,.32), xycoords='figure fraction', fontsize=13)
    plt.annotate('$\\tau_S / \\tau_m^0$= '+\
                 str(round(100*np.float(params['clamp_Ts_ratio']),1))\
                 +'%',(.2,.25), xycoords='figure fraction', fontsize=13)
    plt.annotate('seed ='+str(np.int(params['seed'])),\
                 (.2,.19), xycoords='figure fraction', fontsize=13)

    # time constant ratio calculus

    Tv_ratio_exp = Tv_exp/(np.float(params['Cm'])*np.float(params['Rm'])*1e-6)
    s_Tv_ratio_exp = s_Tv_exp/(np.float(params['Cm'])*np.float(params['Rm'])*1e-6)
    Ts_ratio = Ts/(np.float(params['Cm'])*np.float(params['Rm'])*1e-3)

    # # then subthreshold properties
    fig4 = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.14, left=.11, right=0.99, hspace=.05)
    plt.subplot(311)
    plt.plot(muV[i_muV],muV[i_muV],\
             'k-', lw=8,alpha=.3)
    plt.errorbar(muV[i_muV], muV_exp[i_muV], s_muV_exp[i_muV],\
                 color='k', marker='D')
    plt.ylabel('$\mu_V$ (mV)')
    plt.yticks(np.float(params['clamp_muV'])+np.arange(3)*2-2)
    plt.subplot(312)
    plt.plot(muV[i_muV],np.float(params['clamp_sV'])*np.ones(len(muV)),\
             'k-', lw=8,alpha=.3)
    plt.errorbar(muV[i_muV], sV_exp[i_muV], s_sV_exp[i_muV],\
                 color='k', marker='D')
    plt.yticks(np.float(params['clamp_sV'])+np.arange(3)*2-2)
    plt.ylabel('$\sigma_V$ (mV)')
    plt.subplot(313)
    plt.plot(muV[i_muV], 100.*Ts_ratio[i_muV],\
             'k-', lw=3,alpha=.8, label='$\\tau_S / \\tau_m^0$')
    plt.plot(muV[i_muV],100.*Tv_ratio[i_muV],'k-', lw=3,alpha=.3, label='$\\tau_V / \\tau_m^0$')
    plt.plot(muV[i_muV],100./muGn[i_muV],'k--', lw=2,alpha=.9, label='$\\tau_m / \\tau_m^0$')
    plt.errorbar(muV[i_muV], 100.*Tv_ratio_exp[i_muV], 100.*s_Tv_ratio_exp[i_muV],\
                 color='k', marker='D')
    plt.legend(frameon=False, prop={'size':'x-small'}, loc='upper right')
    plt.yticks([0, 50, 100])
    plt.ylabel('$\\tau / \\tau_m^0$ (%)')
    plt.xlabel('$\mu_V$ (mV)')

    return [fig1, fig2, fig3, fig4, fig5, fig6, fig7]


def sV_effect(filename, crossing=CROSSING,\
         command_channel=2):

    params, t, data, times, exp, time, index, I0, Ts, f, Q,\
       muV, sV, muGn, Tv_ratio, Gs, Fout = load_data(filename)

    fig1 = v_time_trace_plot(t, data[0], times, muGn, '$\sigma_V$ (mV)',\
        '      Varying $\sigma_V$ ',\
        time, crossing=CROSSING)

    try:
        print 'number of steps :', len(index)
    except TypeError:
        return [fig1]
    
    Tstart, duration, Fout, sFout, Rm_exp, El_exp,\
        muV_exp, sV_exp, Tv_exp, ISI_exp =\
         analyze_experiment(params, t, data, times,\
         index, I0, Ts, f, Q, muV, sV, muGn, Tv_ratio, Gs,
         filename_for_saving=time+'_sV.npy',\
         command_channel=command_channel)

    fig2 = recording_prop_plot(params, times, Fout, sFout, Rm_exp, El_exp,\
                muV_exp, sV_exp, Tv_exp, ISI_exp,\
        '      Varying $\sigma_V$',\
        time)

    fig5, fig6, fig7 = extract_all_from_the_data(t, data, times, index, sV,\
            '$\sigma_V$')
            
    muV, sV, muGn, Ts, Tv_ratio, Fout, muV_exp, sV_exp,\
           Tv_exp, s_Fout, s_muV_exp, s_sV_exp, s_Tv_exp = \
        reformat_vec_to_handle_multiple_seed(index, Ts, muV, sV, muGn, Tv_ratio,\
                                         Fout, muV_exp, sV_exp, Tv_exp)


    # # then special quantities
    i_sV = np.argsort(sV)
    
    fig3 = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.14, left=.11, right=0.99)
    plt.suptitle('      Varying $\sigma_V$ at '+time)
    plt.errorbar(sV[i_sV], Fout[i_sV], s_Fout[i_sV],\
                 color='k', marker='D')
    plt.ylabel('$\\nu_{out}$ (Hz)');
    plt.xlabel('$\sigma_V$ (mV)')
    plt.annotate('$\mu_G$= '+str(round(np.float(params['clamp_muGn']),1))\
                 +' ',(.2,.4), xycoords='figure fraction', fontsize=13)
    plt.annotate('$\sigma_V$= '+str(round(np.float(params['clamp_sV']),1))\
                 +'mV',(.2,.32), xycoords='figure fraction', fontsize=13)
    plt.annotate('$\\tau_S / \\tau_m^0$= '+\
                 str(round(100*np.float(params['clamp_Ts_ratio']),1))\
                 +'%',(.2,.25), xycoords='figure fraction', fontsize=13)
    plt.annotate('seed ='+str(np.int(params['seed'])),\
                 (.2,.19), xycoords='figure fraction', fontsize=13)

    # time constant ratio calculus

    Tv_ratio_exp = Tv_exp/(np.float(params['Cm'])*np.float(params['Rm'])*1e-6)
    s_Tv_ratio_exp = s_Tv_exp/(np.float(params['Cm'])*np.float(params['Rm'])*1e-6)
    Ts_ratio = Ts/(np.float(params['Cm'])*np.float(params['Rm'])*1e-3)

    # # then subthreshold properties
    fig4 = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.14, left=.11, right=0.99, hspace=.05)
    plt.subplot(311)
    plt.plot(sV[i_sV],muV[i_sV],\
             'k-', lw=8,alpha=.3)
    plt.errorbar(sV[i_sV], muV_exp[i_sV], s_muV_exp[i_sV],\
                 color='k', marker='D')
    plt.ylabel('$\mu_V$ (mV)')
    plt.yticks(np.float(params['clamp_muV'])+np.arange(3)*2-2)
    plt.subplot(312)
    plt.plot(sV[i_sV],np.float(params['clamp_sV'])*np.ones(len(sV)),\
             'k-', lw=8,alpha=.3)
    plt.errorbar(sV[i_sV], sV_exp[i_sV], s_sV_exp[i_sV],\
                 color='k', marker='D')
    plt.yticks(np.float(params['clamp_sV'])+np.arange(3)*2-2)
    plt.ylabel('$\sigma_V$ (mV)')
    plt.subplot(313)
    plt.plot(sV[i_sV], 100.*Ts_ratio[i_sV],\
             'k-', lw=3,alpha=.8, label='$\\tau_S / \\tau_m^0$')
    plt.plot(sV[i_sV],100.*Tv_ratio[i_sV],'k-', lw=3,alpha=.3, label='$\\tau_V / \\tau_m^0$')
    plt.plot(sV[i_sV],100./muGn[i_sV],'k--', lw=2,alpha=.9, label='$\\tau_m / \\tau_m^0$')
    plt.errorbar(sV[i_sV], 100.*Tv_ratio_exp[i_sV], 100.*s_Tv_ratio_exp[i_sV],\
                 color='k', marker='D')
    plt.legend(frameon=False, prop={'size':'x-small'}, loc='upper right')
    plt.yticks([0, 50, 100])
    plt.ylabel('$\\tau / \\tau_m^0$ (%)')
    plt.xlabel('$\sigma_V$ (mV)')

    return [fig1, fig2, fig3, fig4, fig5, fig6, fig7]

import matplotlib.cm as cm
import scipy.special as sp_spec
from scipy.optimize import minimize,curve_fit

####################################################################################
## Massive transfer function with fit !
## --> taken from the git repository final_transfer_functions
###################################################################################

def tf_kuhn(mu, sigma, Tv, Vthre):
    return .5/Tv*sp_spec.erfc((Vthre-mu)/np.sqrt(2)/sigma)

def scnd_order_pol(p, X, Y):
    return p[0]+p[1]*X+p[2]*Y+p[3]*X**2+p[4]*Y**2+p[5]*X*Y

def poly_func(p, mVm, sVm, muGn):
    return scnd_order_pol(p, mVm, sVm)+p[6]*np.log(muGn)

def func(p, mVm, sVm, Tv, muGn, poly_func): # for the fit, function that should minimize the data
    return tf_kuhn(mVm, sVm, Tv, poly_func(p, mVm, sVm, muGn))


def perform_fit(Fout, mVm, sVm, Tv, muGn):
    # Starting values, naive estimates
    P = [-50e-3, 0., 0., 0., 0., 0., .001]
    
    # Then we want to minimize the firing rate difference !
    def Res(p):
        return np.sum(np.abs(Fout-func(p, mVm, sVm, Tv, muGn, poly_func))/len(Fout))
    
    plsq = minimize(Res,P, method='Powell', tol=1e-6, options={'maxiter':5e4})
    # print plsq
    P = plsq.x

    return P


def threeD_plotting_of_points(muV, sV, muGn, Ts_ratio, Fout, s_Fout):
           
    # then 3d plotting of the firing rate with a 2d input (muV, sV)
    # and a color code for muGn
    FIGS, AXES = [], []
    for ts in np.unique(Ts_ratio):
        i_repet_ts = np.where(Ts_ratio==ts)[0]
        muV2, sV2, muGn2 = muV[i_repet_ts], sV[i_repet_ts], muGn[i_repet_ts]
        Fout2, s_Fout2 = Fout[i_repet_ts], s_Fout[i_repet_ts]
        
        fig = plt.figure(figsize=FIGSIZE) # we have a new figs
        plt.suptitle('$\\tau_S/\\tau_m^0=$ '+\
                     str(np.round(100*ts,1))+' %')
        ax = plt.subplot2grid((6,10),(0,0), colspan=9, rowspan=6, projection='3d')
        ax.view_init(elev=20., azim=210.)
        plt.xlabel('\n$\mu_V$ (mV)')
        plt.ylabel('\n$\sigma_V$ (mV)')
        # colorcode for muGn
        levels = np.unique(muGn2)
        Colors = np.array(['r', 'g', 'b', 'c', 'k']) # no more than 5 values
        ax2 = plt.subplot2grid((6,10),(1,9), rowspan=4)
        cmap = mpl.colors.ListedColormap(Colors[:len(levels)])
        bounds= np.linspace(levels.min()-1, levels.max()+1, len(levels)+1)
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        cb2 = mpl.colorbar.ColorbarBase(ax2, cmap=cmap, norm=norm,
                                    orientation='vertical',
                                    label = '$\mu_G$ / $G_l$',
                                    ticks=levels)


        for muGn3 in levels:
            
            i_repet_muGn = np.where(muGn2==muGn3)[0]
            if muGn.max()>muGn.min():
                r = (muGn3-muGn.min())/(muGn.max()-muGn.min())
            else:
                r=0.

            muV3, sV3 = muV2[i_repet_muGn], sV2[i_repet_muGn]
            Fout3, s_Fout3 = Fout2[i_repet_muGn], s_Fout2[i_repet_muGn]

            for muV4 in np.unique(muV3):

                i_repet_muV = np.where(muV3==muV4)[0]
                
                sV4 = sV3[i_repet_muV]
                Fout4, s_Fout4 = Fout3[i_repet_muV], s_Fout3[i_repet_muV]

                ax.plot(muV4*np.ones(len(sV4)), sV4, Fout4,color=cmap(r,1))
                for ii in range(len(Fout4)): # then errobar manually
                    ax.plot([muV4, muV4], [sV4[ii], sV4[ii]],\
                            [Fout4[ii]+s_Fout4[ii], Fout4[ii]-s_Fout4[ii]],\
                             marker='_', color=cmap(r,1))
                    

        FIGS.append(fig)
        AXES.append(ax)

    return AXES, FIGS


def massive_time_trace_plot(t, times, data, muV, sV, muGn, Tv_ratio, time):
    
    fig1 = plt.figure(figsize=FIGSIZE)
    plt.suptitle('    Varying ($\mu_V, \sigma_V, \mu_G, \\tau_V$) at '+time); sp1 = plt.subplot(111)
    plt.subplots_adjust(bottom=0.1, left=.15, right=0.99, hspace=0.05)
    i_max = min([len(t), int(200000)])
    sp1.plot(t[:i_max], data[0][:i_max], 'b')
    sp1.set_ylabel('$V_m$ (mV)')
    times2 = times[np.where(times<t[i_max-1])]
    for tt in times2: plt.plot([tt,tt], [-80,0], 'r')
    plt.plot([0,t[i_max-1]],[CROSSING, CROSSING], 'g',lw=3,alpha=.3)

    # with its parameters
    fig2 = plt.figure(figsize=FIGSIZE)
    plt.suptitle('    Varying ($\mu_V, \sigma_V, \mu_G, \\tau_V$) at '+time); sp1 = plt.subplot(111)
    plt.subplots_adjust(bottom=0.1, left=.15, right=0.99, hspace=0.05)
    LABELS = ['$\mu_V$', '$\sigma_V$', '$\mu_G^N$', '$\\tau_V^N$']
    i = 0
    for QUANT in [muV, sV, muGn, Tv_ratio]:
        plt.subplot(411+i)
        plt.plot(times[::2], QUANT[:len(times[::2])], 'kD-')
        plt.yticks(np.unique(QUANT))
        # plt.yticks(np.round(np.linspace(round(QUANT.min())-.1,\
        #     round(QUANT.max())+.1, 3, endpoint=True)))
        # plt.ylim([round(QUANT.min())-2, round(QUANT.max())+2])
        plt.xlabel('time (s)'); plt.ylabel(LABELS[i])
        plt.xlim([t[0], t[-1]])
        i+=1

    return fig1, fig2
    
def muV_sV_muGn_Ts_analysis(filename, crossing=CROSSING,\
         command_channel=2):

    params, t, data, times, exp, time, index, I0, Ts, f, Q,\
       muV, sV, muGn, Tv_ratio, Gs, Fout = load_data(filename)

    ### first we plot the full experiment
    fig1, fig2 = massive_time_trace_plot(t, times, data, muV, sV, muGn, 100*Tv_ratio, time)
    
    try:
        print 'number of steps :', len(index)
    except TypeError:
        return [fig1, fig2]
    
    Tstart, duration, Fout, sFout, Rm_exp, El_exp,\
        muV_exp, sV_exp, Tv_exp, ISI_exp =\
         analyze_experiment(params, t, data, times,\
         index, I0, Ts, f, Q, muV, sV, muGn, Tv_ratio, Gs,
         filename_for_saving=time+'_muV_sV_muGn_Tv.npy',\
         command_channel=command_channel)

    fig3 = recording_prop_plot(params, times, Fout, sFout, Rm_exp, El_exp,\
                muV_exp, sV_exp, Tv_exp, ISI_exp,\
        '    Varying ($\mu_V, \sigma_V, \mu_G, \\tau_V$)', time)
        

    ### then we plot the grid 
    levels = np.unique(muGn)
    fig4 = plt.figure(figsize=FIGSIZE)
    plt.suptitle('    Varying ($\mu_V, \sigma_V, \mu_G, \\tau_V$) at '+time); sp1 = plt.subplot(111)
    ax = plt.subplot2grid((1,7),(0,0), colspan=6)
    Colors = np.array(['r', 'g', 'b', 'c', 'k', 'y', 'r', 'b', 'k'])
    cmap = mpl.colors.ListedColormap(Colors[:len(levels)])
    bounds= np.linspace(levels.min()-1, levels.max()+1, len(levels)+1)
    norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
    Colors = np.array([Colors[np.where(mm==levels)] for mm in muGn])
    for i in range(len(muV_exp)):
        if (Fout.max()-Fout.min())>0:
            ss = 50.*(Fout[i]-Fout.min())/(Fout.max()-Fout.min())
        else:
            ss = 20.
        plt.scatter(muV_exp[i], sV_exp[i], color=Colors[i], marker='*',s=ss)
    plt.scatter(muV, sV, color='k', marker='D')
    plt.xlabel('$\mu_V$ (mV)');plt.ylabel('$\sigma_V$ (mV)')
    ax2 = plt.subplot2grid((1,7),(0,6), rowspan=1)
    cb2 = mpl.colorbar.ColorbarBase(ax2, cmap=cmap, norm=norm,
                                    label = '$\mu_G$ / $G_l$',
                                    ticks=levels)
    plt.tight_layout()
    
    muV, sV, muGn, Ts, Tv_ratio, Fout, muV_exp, sV_exp,\
           Tv_exp, s_Fout, s_muV_exp, s_sV_exp, s_Tv_exp = \
        reformat_vec_to_handle_multiple_seed(index, Ts, muV, sV, muGn, Tv_ratio,\
                                         Fout, muV_exp, sV_exp, Tv_exp)

    Ts_ratio = 1e3*Ts/(float(params['Rm'])*float(params['Cm'])) # 1e3*ms/us
    Ts_ratio = np.round(Ts_ratio, 2)

    AXES, FIGS = threeD_plotting_of_points(muV, sV, muGn, Ts_ratio, Fout, s_Fout)
    FIGS.append(fig1);FIGS.append(fig2);FIGS.append(fig3);FIGS.append(fig4)

    return FIGS


def analysis(filename, return_fig=True, crossing=CROSSING,\
             command_channel=2):
    """
    take the filename of this and returns the classic analysis
    associated to this protocol !
    """
    exp, time = ep.get_experiment_name_and_time(filename)
    print exp, time
    if exp=='TF_GTOT_EFFECT_ON_SPIKING':
        figs = muGn_effect(filename, crossing=crossing,\
                           command_channel=command_channel)
    elif exp=='TF_TV_EFFECT_ON_SPIKING':
        figs = Tv_effect(filename, crossing=crossing,\
                           command_channel=command_channel)
    elif exp=='TF_GTOT_AND_TV_EFFECT_ON_SPIKING':
        figs = muGn_and_Tv_effect(filename, crossing=crossing,\
                           command_channel=command_channel)
    elif exp=='TF_MUV_EFFECT_ON_SPIKING':
        figs = muV_effect(filename, crossing=crossing,\
                           command_channel=command_channel)
    elif exp=='TF_SV_EFFECT_ON_SPIKING':
        figs = sV_effect(filename, crossing=crossing,\
                           command_channel=command_channel)
    elif exp=='TF_MASSIVE_MV_SV_TV_MG':
        figs = muV_sV_muGn_Ts_analysis(filename, crossing=crossing,\
                           command_channel=command_channel)
    else:
        print 'filename not recognized'
        exp, time = ep.get_experiment_name_and_time(filename) # just for time
        t, data = ep.get_signals_continuous(filename)
        fig = plt.figure(figsize=FIGSIZE)
        plt.title(exp+' at '+time)
        sp1 = plt.subplot(111)
        i_max = max([len(t), int(50000)])
        sp1.plot(t[:i_max], data[0][:i_max], 'b')
        sp1.set_ylabel('$V_m$ (mV)')
        figs = [fig]

    return figs
        
    


