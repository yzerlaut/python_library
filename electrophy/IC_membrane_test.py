"""
Membrane Test in Currrent clamp (to estimate Rm and Cm)
usually after access resistance compensation
"""
import numpy as np
import matplotlib.pylab as plt
import sys
sys.path.append('../')#/home/yann/work/python_library/')
from my_graph import set_plot
from scipy.optimize import minimize
import elphy_to_python as ep # to be deleted

FIGSIZE = (7,5) # global figsize, common to all generated figures


### --- SINGLE COMPARTMENT APPROXIMATION
def make_fit_for_single_comp(t, v_trace, DiA, t0, maxiter=10000,\
                        initial_value=(-70.,5.,1./30.)):
    """ units: ms, mV, pA, """
    H = np.array([0. if tt<t0 else 1. for tt in t]) # heaviside function
    def step_response_1_comp(A, B, C):
        return A+B*(1-np.exp(-C*(t-t0)))*H
    def to_minimize(P):
        return np.mean(np.abs(v_trace-step_response_1_comp(*P)))
    bounds = ((-100., -40.), (-40.,40), (1./5e2,1.))
    res = minimize(to_minimize, initial_value,\
                 method='L-BFGS-B', bounds=bounds,\
                 options={'maxiter':maxiter})
    v_fit = step_response_1_comp(*res.x)
    A, B, C = res.x
    # translated into membrane parameters (units: Mohm, mV, pF)
    Rm, El, Cm = np.round(1e3*B/DiA,1), np.round((A+B),1), np.round(DiA/B/C,1) 
    return [Rm, El, Cm], v_fit

### --- DOUBLE COMPARTMENT APPROXIMATION
def make_fit_for_double_comp(t, v_trace, DiA, t0, maxiter=100000,\
         initial_value=(-70.,5.,1./30.,5.,1./30.)):
    """ units: ms, mV, pA, """

    H = np.array([0. if tt<t0 else 1. for tt in t]) # heaviside function
    def step_response_2_comp(A, B, C, D, E):
        return A+B*(1-np.exp(-C*(t-t0)))*H+D*(1-np.exp(-E*(t-t0)))*H
    def to_minimize(P):
        return np.mean(np.abs(v_trace-step_response_2_comp(*P)))
    bounds = ((-100., -40.), (-40.,40), (1./5e2,1.), (-40.,40), (1./5e2,1.))
    res = minimize(to_minimize, initial_value,\
                 method='L-BFGS-B', bounds=bounds,\
                 options={'maxiter':maxiter})
    v_fit = step_response_2_comp(*res.x)
    A, B, C, D, E = res.x
    # translated into membrane parameters (units: Mohm, mV, pF)
    ### to be done, not trivial (need to pursue the matrix calculus) !!!
    RmS, CmS, Ra, RmD, CmD = 0, 0, 0, 0, 0
    return [RmS, CmS, Ra, RmD, CmD], v_fit


## calculus for the 2 compartment model
def func_2comp_full(t, g1, g2, ga, C1, C2, El, I0):
    # units : ms, nS, pF, mV, pA
    return (C2*((g2 + ga)/C2 - (C2*g1 + C1*g2 + C1*ga + C2*ga +  np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/(2.*C1*C2)        )*(((C2*g1 + C1*g2 + C1*ga + C2*ga -              np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))*           ((C1*El*g2*((g2 + ga)/C2 -                   (C2*g1 + C1*g2 + C1*ga + C2*ga -                      np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/                   (2.*C1*C2)))/              np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)) -              (ga*(El*g1 + I0))/              np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))))/         (2.*C1*C2*((g1*g2)/(C1*C2) + (g1*ga)/(C1*C2) + (g2*ga)/(C1*C2))) +         (-((C1*El*ga)/              np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))) +            (C1*C2*El*((g2 + ga)/C2 - (C2*g1 + C1*g2 + C1*ga + C2*ga -                    np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/                 (2.*C1*C2)))/            np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)) -            ((C2*g1 + C1*g2 + C1*ga + C2*ga -                 np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))*              ((C1*El*g2*((g2 + ga)/C2 -                      (C2*g1 + C1*g2 + C1*ga + C2*ga -                         np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 -                           4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/(2.*C1*C2)))/                 np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)) -                 (ga*(El*g1 + I0))/                 np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))))/            (2.*C1*C2*((g1*g2)/(C1*C2) + (g1*ga)/(C1*C2) + (g2*ga)/(C1*C2))))/         np.exp(((C2*g1 + C1*g2 + C1*ga + C2*ga +                 np.sqrt(C2**2*g1**2 - 2*C1*C2*g1*g2 + C1**2*g2**2 - 2*C1*C2*g1*ga + 2*C2**2*g1*ga +                   2*C1**2*g2*ga - 2*C1*C2*g2*ga + C1**2*ga**2 + 2*C1*C2*ga**2 + C2**2*ga**2))*t)/            (2.*C1*C2))))/ga + (C2*((g2 + ga)/C2 -         (C2*g1 + C1*g2 + C1*ga + C2*ga -            np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/(2.*C1*C2)        )*(((C2*g1 + C1*g2 + C1*ga + C2*ga +              np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))*           (-((C1*El*g2*((g2 + ga)/C2 -                     (C2*g1 + C1*g2 + C1*ga + C2*ga +                        np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))                       )/(2.*C1*C2)))/                np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))) +              (ga*(El*g1 + I0))/              np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))))/         (2.*C1*C2*((g1*g2)/(C1*C2) + (g1*ga)/(C1*C2) + (g2*ga)/(C1*C2))) +         ((C1*El*ga)/np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)) -            (C1*C2*El*((g2 + ga)/C2 - (C2*g1 + C1*g2 + C1*ga + C2*ga +                    np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/                 (2.*C1*C2)))/            np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)) -            ((C2*g1 + C1*g2 + C1*ga + C2*ga +                 np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))*              (-((C1*El*g2*((g2 + ga)/C2 -                        (C2*g1 + C1*g2 + C1*ga + C2*ga +                           np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 -                             4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/(2.*C1*C2)))/                   np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))) +                 (ga*(El*g1 + I0))/                 np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))))/            (2.*C1*C2*((g1*g2)/(C1*C2) + (g1*ga)/(C1*C2) + (g2*ga)/(C1*C2))))/         np.exp(((C2*g1 + C1*g2 + C1*ga + C2*ga -                 np.sqrt(C2**2*g1**2 - 2*C1*C2*g1*g2 + C1**2*g2**2 - 2*C1*C2*g1*ga + 2*C2**2*g1*ga +                   2*C1**2*g2*ga - 2*C1*C2*g2*ga + C1**2*ga**2 + 2*C1*C2*ga**2 + C2**2*ga**2))*t)/ (2.*C1*C2))))/ga


def analyze(exp, time, t, data, params,
            command_channel=1, delay_before_Rm_eval=100e-3):


    mean_v_response = np.zeros(t.size)
    mean_i = np.zeros(t.size)
    
    for i in range(len(data)):
        mean_v_response += data[i][0]/(len(data))
        mean_i += data[i][command_channel]/(len(data))

    dt = t[1]-t[0]
    DI = np.abs(np.diff(mean_i[5:])).max() # pA, pulse
    # we find where the pulse start !
    i1 = np.where(np.abs(np.diff(mean_i[5:]))>.6*DI)[0]
    step_times = np.where(np.abs(np.diff(mean_i[5:]))>.6*DI)[0]
    if len(step_times)==2:
        [i1, i2] = step_times
    else:
        if len(step_times)>2:
            [i1, i2] = step_times[0], step_times[1]
        else:
            i1, i2= 4000, 8000
        print '================================================='
        print '--------> problem with the pulse recognition '
        print '================================================='

    # then we discard the loading time of the membrane
    i1_bis = i1+int(delay_before_Rm_eval/dt)

    i_base = mean_i[5:i1-1].mean()
    i_end = mean_i[i1+2:i2-2].mean() # average over last 10 points and traces !
    v_base = mean_v_response[5:i1-1].mean()
    v_end = mean_v_response[i1_bis+1:i2-1].mean()
    

    v_fit = mean_v_response[i1:i2]
    t_fit = t[:i2]-t[0]
    
    success_flag_1comp=False
    print "Single compartment fit :"
    try:
        [Rm, El, Cm], v_fit_1comp = make_fit_for_single_comp(\
            t[:i2], mean_v_response[:i2],i_end-i_base, t[i1])
        success_flag_1comp=True
        print "--------> Ok !"
    except RuntimeError:
        print "Fit for the Membrane Test not achieved !"

    success_flag_2comp=False
    print "Double compartment fit :"
    try:
        [RmS, CmS, Ra, RmD, CmD], v_fit_2comp = make_fit_for_double_comp(\
            t[:i2], mean_v_response[:i2],i_end-i_base, t[i1])
        success_flag_2comp=True
        print "--------> Ok !"
    except RuntimeError:
        print "Fit for the Membrane Test not achieved !"

    return exp, time, t, data, params, Rm, El, Cm,\
        t_fit, v_fit_1comp, RmS, CmS, Ra, RmD, CmD, v_fit_2comp,\
        mean_v_response, mean_i


def plot(filename):

    exp, time, t, data, params = load(filename)

    exp, time, t, data, params, Rm, El, Cm, t_fit, v_fit_1comp,\
         RmS, CmS, Ra, RmD, CmD, v_fit_2comp, mean_v_response, mean_i = \
                        analyze(exp, time, t, data, params)

    fig = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=.16, right=.97, left=.17)
    sp1 = plt.subplot2grid((3,1), (0,0), rowspan=2)
    sp2 = plt.subplot2grid((3,1), (2,0))
    plt.suptitle('I-Clamp Membrane Test : '+time)
      
    for i in range(len(data)):
        sp1.plot(t, data[i][0], color='k', lw=0.5, alpha=.2)

    sp1.plot(t, mean_v_response, color='k', lw=1)
    sp2.plot(t, mean_i, 'k', lw=1)

    sp1.plot(t_fit, v_fit_1comp, 'b--', lw=4, label='Single comp.')
    fig.text(.2,.25,'Single comp. :  $R_m$='+str(Rm)+'M$\Omega$, '+\
         '  Cm='+str(Cm)+'pF'+'  El='+str(El)+'mV',
         fontsize=13,backgroundcolor='lightgray')

    sp1.plot(t_fit, v_fit_2comp, 'r-', lw=1, label='Double comp.')
    #     fig.text(.05,.13,'Double comp. :  $R_m^1$='+\
    #              str(round(RmS,1))+'M$\Omega$, $R_m^2$='+\
    #              str(round(RmD,1))+'M$\Omega$, $R_i$='+\
    #              str(round(Ra,1))+'M$\Omega$, $C_m^1$='+\
    #              str(round(CmS,1))+'pF, $C_m^2$='+\
    #              str(round(CmD,1))+'pF',
    #              fontsize=11,backgroundcolor='lightgray')

    set_plot(sp1, ylabel='$V_m$ (mV)', spines=['left'])
    set_plot(sp2, ylabel='current (pA)', xlabel='time (ms)')
    sp1.legend(prop={'size':'x-small'}, loc='best')

    return fig, sp1, sp2

def load(filename):
    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    t, data = ep.get_signals_episode(filename)
    params = ep.get_metadata(filename) # metadata
    return exp, time, t, data, params

if __name__=='__main__':
    import argparse
    parser=argparse.ArgumentParser(description=
     """ 
     Analysis of the Voltage Clamp protocols to determine the quality
     of the cellular access and the membrane properties
     """
    ,formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("FILE", nargs='*',\
      help="FILE to be included in the analysis ",
      default=['/media/yann/DATA_EqAlain/files/DATA/2015_7_2/21_48_48_IC_MEMBRANETEST1.DAT'])

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    
    fig, p1, sp1 = plot(args.FILE[0])
    plt.show()
