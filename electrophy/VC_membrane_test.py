import numpy as np
import matplotlib.pylab as plt
import sys
sys.path.append('../')#/home/yann/work/python_library/')
from my_graph import set_plot
from scipy.optimize import curve_fit
import elphy_to_python as ep # to be deleted

FIGSIZE = (7,5) # global figsize, common to all generated figures

def analyze(exp, time, t, data, params,
            t_pre=2, t_post=13,
            peak_window = [10./100., 99./100.],
            command_channel=2):
    """
    arguments :
    -- exp, time, t, data, params
    optional :
    -- t_pre and t_post set the windows of sampling of the stationary currents:
       Ibase = I[t2-t_pre,t2], Iend=[t3-t_post,t3]
       they are also usefull for the visualization
       we visualize : [t2-t_pre, t3-t_post]
    -- max and min of peak to fit the time constant
    -- command channel !!
    """

    dt = t[1]-t[0]

    if params.has_key('t4'): # if everything recorded !
        t1, t2, t3, t4 = float(params['t1']), float(params['t2']),\
             float(params['t3']), float(params['t4'])
        amp = float(params['pulse_amplitude'])
    else:
        # default values
        t1, t2, t3, t4 = 20, 40 , 60, 80
        amp = -5

    # mean response
    mean_i_response = np.zeros(t.size)
    for i in range(len(data)):
        mean_i_response += data[i][0]/(len(data))

    Ileak = mean_i_response[:int(t1/dt)-1].mean()
    
    i_interest = mean_i_response[int((t2-t_pre)/dt):int((t3-t_post)/dt)]
    t_interest = t[int((t2-t_pre)/dt):int((t3-t_post)/dt)]

    # now calculus of all the membrane parameters
    Ibase = mean_i_response[int((t2-t_pre)/dt):int((t2)/dt)].mean()
    Iend = mean_i_response[int((t3-t_post)/dt):int((t3)/dt)].mean()
    DI_stat = Iend-Ibase
    Ijump = i_interest.min()-Ibase

    ## multiplt possibilites here, we take 2/3 of the min amplitude by default
    i_min = np.argmin(abs(i_interest-Iend-(i_interest.min()-Iend)*peak_window[1]))
    i_max = np.argmin(abs(i_interest[i_min:]-Iend-(i_interest[i_min:].min()-Iend)*peak_window[0]))+i_min
    # curve fitting
    t_fit, i_fit = t_interest[i_min:i_max], i_interest[i_min:i_max]
    t_fit = t_fit-t_fit[0] # to start from zero !
    i_fit = (i_fit-i_fit.min())/(i_fit.max()-i_fit.min()) # normalized from 0 to 1 !
    def func(x, a): # will take a normalized current to have less parameters
        return 1-np.exp(-a*x)
    y = func(t_fit, 1) # guess
    success_flag=False
    try:
        popt, pcov = curve_fit(func, t_fit, i_fit)
        if popt[0]!=0:
            Tau=1./popt[0]
        else:
            Tau=1e9
        success_flag=True
        no_norm_i_fit = i_interest[i_min:i_max].min()+func(t_fit, popt[0])*(i_interest[i_min:i_max].max()-i_interest[i_min:i_max].min())
    except RuntimeError:
        print "Fit for the Membrane Test not achieved !"
        Tau=1e9
    # now that we have Tau, we can do the calculus
    Tintegration = t3-t_post-t2-3*dt # we remove the 2 first steps
    integral_response = \
      np.trapz(mean_i_response[int(t2/dt)+2:int((t3-t_post)/dt)], dx=dt)\
      -Ibase*Tintegration
    integral_resist = DI_stat*(Tintegration+Tau*(np.exp(-Tintegration/Tau)-1))
    integral_capa = integral_response-integral_resist
    if integral_capa!=0:
        Rs=Tau*amp/integral_capa*1000.0  # ms*mV / ms*pA *1000 -> Mohm
    else:
        Rs=1e6
    if DI_stat!=0:
        Rm=amp/DI_stat*1000-Rs #{ mV/pA*1000 -> Mohm  }
    else:
        Rm=1e6 
    Cm=Tau*(Rs+Rm)/(Rm*Rs)*1000 # {ms/Mohm*1000-> pF }

    if success_flag:
        return exp, time, t, data, params, t_interest, i_interest,\
            t_fit+t_interest[i_min], no_norm_i_fit, Rs, Ileak, Rm, Cm
    else:
        return exp, time, t, data, params, t_interest, i_interest,\
            t_fit+t_interest[i_min], 0*t_fit, Rs, Ileak, Rm,Cm


def load(filename):
    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    t, data = ep.get_signals_episode(filename)
    params = ep.get_metadata(filename) # metadata
    return exp, time, t, data, params

def plot(filename,\
         peak_window = [10./100., 95./100.]):

    exp, time, t, data, params = load(filename)
    exp, time, t, data, params, t_interest, i_interest,\
        t_fit, i_fit, Rs, Ileak, Rm, Cm = analyze(exp, time, t, data, params)
    
    fig = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=.17, right=.97, left=.15)
    plt.suptitle('Membrane Test : '+time)
    p1 = plt.subplot(111) # main figure
    sp1 = plt.axes([0.58,0.34,0.3,0.3]) # inset for full trace
    
     
    if params.has_key('t4'): # if everything recorded !
        t1, t2, t3, t4 = float(params['t1']), float(params['t2']),\
             float(params['t3']), float(params['t4'])
        amp = float(params['pulse_amplitude'])
    else:
        # default values
        t1, t2, t3, t4 = 20, 40 , 60, 80
        amp = -5

    # mean response
    mean_i_response = np.zeros(t.size)
    dt = t[1]-t[0]
    for i in range(len(data)):
        sp1.plot(t, data[i][0], color='k', lw=0.5, alpha=.5)
        mean_i_response += data[i][0]/(len(data))
    # calculus of the leak current
    leak = mean_i_response[:int(t1/dt)-1].mean()
    

    # for plotting purpose, we draw the input
    xstim = np.array([10,t1,t1,t2,t2,t3,t3,t4,t4,t4+t1-10])
    ystim = np.array([0,0,-amp/2.,-amp/2.,amp/2.,amp/2.,-amp/2.,-amp/2.,0,0])
    ystim = ystim/(ystim.max()-ystim.min())*0.3*(mean_i_response.max()-mean_i_response.min())
    ystim += mean_i_response.min()-0.3*(mean_i_response.max()-mean_i_response.min())

    sp1.plot(xstim, ystim, 'k', lw=2)

    p1.plot(t_interest, i_interest, 'k', lw=2)
    Ibase = i_interest[:10].mean()
    p1.fill_between(t_interest, i_interest, np.ones(len(t_interest))*Ibase, color='lightgray')
    p1.annotate(' Ileak:\n'+str(round(leak))+'pA', (.15,.62),\
            xycoords='figure fraction')
    p1.annotate('  Rs:\n'+str(round(Rs))+'M$\Omega$', (.15,.47), xycoords='figure fraction')    
    p1.annotate('  Rm:\n'+str(round(Rm))+'M$\Omega$',(.15,.32), xycoords='figure fraction')    
    p1.annotate('  Cm:\n'+str(round(Cm))+'pF', (.15,.17), xycoords='figure fraction')    
    p1.plot(t_fit, i_fit, 'k--', lw=3)

    set_plot(sp1, xlabel='time (ms)', spines=['left', 'bottom'])
    set_plot(p1, ylabel='current (pA)', xlabel='time (ms)')

    return fig, p1, sp1

def add_data(filename, fig, p1, sp1, color='r', label='',\
             peak_window=[]):

    exp, time, t, data, params = load(filename)
    exp, time, t, data, params, t_interest, i_interest,\
       t_fit, i_fit, Rs, Ileak, Rm, Cm = analyze(exp, time, t, data, params, peak_window=peak_window)

    p1.plot(t_interest, i_interest, color=color, lw=2, label=label)
    for i in range(len(data)):
        sp1.plot(t, data[i][0], color=color, lw=0.5, alpha=.5)
    p1.plot(t_fit, i_fit, '--', color=color, lw=3)

    # then adjusting the plot
    ymin, ymax = p1.get_yaxis().get_view_interval()
    if ymin>i_interest.min():
        ymin = i_interest.min()
    if ymax<i_interest.max():
        ymax = i_interest.max()
    set_plot(p1, ylabel='current (pA)', xlabel='time (ms)', ylim=[ymin, ymax])
    # then adjusting the plot
    ymin, ymax = sp1.get_yaxis().get_view_interval()
    if ymin>i_interest.min():
        ymin = i_interest.min()
    if ymax<i_interest.max():
        ymax = i_interest.max()
    set_plot(sp1, xlabel='time (ms)', spines=['left', 'bottom'], ylim=[ymin, ymax])
    

def load_and_analyze(filename, return_fig=False):

    exp, time, t, data, params = load(filename)
    exp, time, t, data, params, t_interest, i_interest,\
        t_fit, i_fit, Rs, Ileak, Rm, Cm = analyze(exp, time, t, data, params)
    
    if return_fig:
        fig, p1, sp1 = plot(filename)
        return Rs, Ileak, Rm, Cm, fig, p1, sp1
    else:
        return Rs, Ileak, Rm, Cm

    

if __name__=='__main__':
    import argparse
    parser=argparse.ArgumentParser(description=
     """ 
     Analysis of the Voltage Clamp protocols to determine the quality
     of the cellular access and the membrane properties
     """
    ,formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("FILES", nargs='*',\
      help="FILE(S) to be included in the analysis ",
      default=['/media/yann/DATA_EqAlain/files/DATA/2015_7_2/17_12_38_VC_MEMBRANETEST1.DAT'])
    parser.add_argument("--LABELS", "-l", nargs='*',\
      help="LABEL(S) for each of the FILE(S) included in the analysis ",
      default=[''])

    parser.add_argument("--peak_window", nargs=2,\
                        help="fraction of the peak considered for the fit (in percentage) \n"+\
                        'default= [0.1, 0.95]',type=float,
                        default=[0.1, 0.95])

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    
    if (args.LABELS==['']) or (len(args.LABELS)!=len(args.FILES)):
        args.LABELS = ['' for i in range(len(args.FILES))]
        if args.verbose:
            print 'no labels, or wrong label number'

    COLORS = np.array(['r', 'b', 'g', 'c', 'y'])[np.arange(len(args.FILES)-1)]

    # we create the first plot
    fig, p1, sp1 = plot(args.FILES[0], peak_window=args.peak_window)
    
    # then we add the other ones
    for i in range(1, len(args.FILES)):
        add_data(args.FILES[i], fig, p1, sp1,\
            color=COLORS[i-1], label=args.LABELS[i],\
                 peak_window=args.peak_window)

    if (args.LABELS[0]!=''):
        p1.plot([40,40], [-50,-50], 'k', lw=2, label=args.LABELS[0])
        p1.legend(frameon=False, prop={'size':'xx-small'})

    plt.show()
