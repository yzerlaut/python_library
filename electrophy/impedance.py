import numpy as np
import matplotlib.pylab as plt
import sys
sys.path.append('../')#/home/yann/work/python_library/')
from my_graph import set_plot
import elphy_to_python as ep # to be deleted

FIGSIZE = (7,5) # global figsize, common to all generated figures

###### --- Multiple Sinusoids Analysis
def get_impedance(filename):
    """
    here we will actually do the fit of the sinuisoids one by 
    one, but by waiting we load the analysis performed by 
    Elphy
    """
    analyzed = filename.replace('1.DAT','_analysed.txt')
    try :
        dd = np.loadtxt(analyzed, unpack=True)
        ss = np.argsort(dd[0])
        freq, Imped = dd[0][ss], dd[1][ss]
        phase = dd[2][ss]%(2.*np.pi)
        return freq, Imped, phase
    except IOError:
        print '================================================'
        print 'File', filename
        print 'not pre-analized in Elphy'
        print '================================================'
        return None
    

def slope_analysis(freq, Imped, sliding_window=3):
    """
    we use a sliding window to study the slope as a function 
    of an increasing frequency
    """
    x,y = [], []
    for i in range(sliding_window,len(freq)-sliding_window+1):
        # linear fitting
        pol = np.polyfit(np.log(freq[i-sliding_window:i+sliding_window]),
                         np.log(Imped[i-sliding_window:i+sliding_window]),1)
        x.append(freq[i])
        y.append(pol[0])
    return x, y
    
def load(filename):
    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    t, data1 = ep.get_signals_continuous(filename)
    params = ep.get_metadata(filename) # metadata
    # we prepare the new format
    data = params.copy()
    data['experiment'], data['time'] = exp, time
    # current clamp here
    data['t'], data['v'] = t, data1[0]
    data['i_inj'], data['i_cmd'] = data1[1], data1[2]

    I = get_impedance(filename)
    if I is not None:
        freq, Imped, phase = I
        return data, freq, Imped, phase
    else:
        return data, np.ones(2), np.ones(2), np.ones(2)
        


def plot(filename, label=''):
    """
    here we generate 3 plots:
    1) a sample of the traces
    2) the modulus of impedance plot (log-log) with a slope analysis
    3) the phase of the impedance plot (log-linear)
    """
    data, freq, Imped, phase = load(filename)
    x, y = slope_analysis(freq, Imped)

    fig_trace = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=.15, left=.15)
    plt.suptitle('input imped. I-Clamp : '+data['time'])
    ax1 = plt.subplot2grid((3,1), (0,0), rowspan=2)
    plt.plot(data['t'][10000:], data['v'][10000:], 'k')
    set_plot(ax1, spines=['left'], ylabel='Vm (mV)')
    ax2 = plt.subplot2grid((3,1), (2,0), rowspan=2)
    plt.plot(data['t'][10000:], data['i_cmd'][10000:], 'k')
    set_plot(ax2, xlabel='time (s)', ylabel='I (pA)')

    fig_imped = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=.15, left=.15)
    plt.suptitle('input imped. : '+data['time'])
    ax1 = plt.subplot(111)
    ax1.loglog(freq, Imped, 'kD-')
    ax1.set_xlabel('frequency (Hz)')
    ax1.set_ylabel('Impedance modulus (M$\Omega$)')
    ax1.grid()
    plt.xlim([0.2,1500])
    sp1 = plt.axes([0.3,0.3,0.2,0.2]) # inset for full trace
    sp1.semilogx(x, y, 'k-')
    sp1.set_xticks([1,10,100,1000])
    plt.yticks([-1,-.75,-.5,-.25,0], ['-1','','-0.5','','0'])
    plt.xlim([0.2,1500])
    sp1.set_xlabel('f (Hz)')
    sp1.set_ylabel('log-log slope')
    sp1.grid()

    fig_phase = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=.15, left=.15)
    ax2 = plt.subplot(111)
    ax2.semilogx(freq, phase, 'kD-', label=label)
    plt.xlabel('frequency (Hz)')
    plt.ylabel('phase shift (Rd)')
    plt.yticks([0,np.pi/2.,np.pi],\
               ["0","$\pi$/2","$\pi$"])
    plt.xlim([0.2,1500])
    ax2.set_ylim([-.2,np.pi+.01])

    return fig_trace, fig_imped, fig_phase, ax1, ax2, sp1


    
def add_data(filename, ax1, ax2, sp1, label='', color='r'):

    data, freq, Imped, phase = load(filename)
    x, y = slope_analysis(freq, Imped)

    ax1.loglog(freq, Imped, 'D-', label=label, color=color)
    sp1.semilogx(x, y, label=label, color=color)
    ax2.semilogx(freq, phase, 'D-', label=label, color=color)
    ax2.set_ylim([-.2,np.pi+.01])
    


if __name__=='__main__':
    import argparse
    parser=argparse.ArgumentParser(description=
     """ 
     Analysis of the Voltage Clamp protocols to determine the quality
     of the cellular access and the membrane properties
     """
    ,formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("FILES", nargs='*',\
      help="FILE(S) to be included in the analysis ",
      default=['/media/yann/DATA_EqAlain/files/DATA/2015_9_15/16_43_35_MULTIPLE_SIN1.DAT_analyzed'])
    parser.add_argument("--LABELS", "-l", nargs='*',\
      help="LABEL(S) for each of the FILE(S) included in the analysis ",
      default=[''])
    parser.add_argument("-s", "--SAVE", help="save figures on Desktop as SVG",
                        action="store_true")

    args = parser.parse_args()
    
    if (args.LABELS==['']) or (len(args.LABELS)!=len(args.FILES)):
        args.LABELS = ['' for i in range(len(args.FILES))]

    COLORS = np.array(['r', 'b', 'g', 'c', 'y'])[np.arange(len(args.FILES)-1)]

    # we create the first plot
    fig_trace, fig_imped, fig_phase, ax1, ax2, sp1 = plot(args.FILES[0], label=args.LABELS[0])
    
    # then we add the other ones
    for i in range(1, len(args.FILES)):
        add_data(args.FILES[i], ax1, ax2, sp1, color=COLORS[i-1], label=args.LABELS[i])

    if (args.LABELS[0]!=''):
        ax2.legend(frameon=False, loc='upper left', prop={'size':'x-small'})

    if args.SAVE:
        fig_imped.savefig('/home/yann/Desktop/fig_imped.svg', format='svg')
        fig_phase.savefig('/home/yann/Desktop/fig_phase.svg', format='svg')
    else:
        plt.show()

