import numpy as np
import matplotlib.pylab as plt
import sys
sys.path.append('../')#/home/yann/work/python_library/')
from my_graph import set_plot
import elphy_to_python as ep # to be deleted


FIGSIZE = (7,5) # global figsize, common to all generated figures

def load(filename):
    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    t, data1 = ep.get_signals_continuous(filename)
    params = ep.get_metadata(filename) # metadata
    # we prepare the new format
    data = params.copy()
    data['experiment'], data['time'] = exp, time
    # current clamp here
    data['t'], data['v'] = t, data1[0]
    data['i_inj'], data['i_cmd'] = data1[1], data1[2]
    return data

def find_and_analyze_spikes(data, crossing=-40, window=[-2,10]):
    """
    crossing : spike detection threshold in mV
    window : extent of the data we look at around this
    """
    spike_indexes = np.where(\
        np.sign(data['v'][:-1]-crossing)<np.sign(data['v'][1:]-crossing))
    dt = data['t'][1]-data['t'][0]
    dv_dt = np.diff(data['v'])/dt/1e3
    v = .5*(data['v'][1:]+data['v'][:-1]) # recentering of the data !!
    indexes_window = np.arange(int(window[0]*1e-3/dt), int(window[1]*1e-3/dt))
    V, DV = [], []
    for index in spike_indexes[0]:
        V.append(v[index+indexes_window])
        DV.append(dv_dt[index+indexes_window])
    return V, DV

def plot(filename, label=''):
    """
    here we generate 3 plots:
    1) a sample of the traces
    2) the modulus of impedance plot (log-log) with a slope analysis
    3) the phase of the impedance plot (log-linear)
    """
    data = load(filename)
    V, DV = find_and_analyze_spikes(data)

    fig_trace = plt.figure(figsize=FIGSIZE)
    plt.suptitle(data['experiment']+' : '+data['time'])
    plt.subplots_adjust(bottom=.15, left=.15)
    ax1 = plt.subplot2grid((3,1), (0,0), rowspan=2)
    plt.plot(data['t'][10000:], data['v'][10000:], 'k')
    set_plot(ax1, spines=['left'], ylabel='Vm (mV)')
    ax2 = plt.subplot2grid((3,1), (2,0), rowspan=2)
    plt.plot(data['t'][10000:], data['i_cmd'][10000:], 'k')
    set_plot(ax2, xlabel='time (s)', ylabel='I (pA)')

    fig_ps = plt.figure(figsize=FIGSIZE)
    plt.suptitle('A.P. phase space')
    plt.subplots_adjust(bottom=.15, left=.15)
    ax1 = plt.subplot(111)
    for v, dv in zip(V, DV):
        ax1.plot(v, dv, 'k-', lw=.5)
    ax1.set_xlabel('V (mV)')
    ax1.set_ylabel('dV/dt (mV/ms)')
    ax1.grid()

    return fig_trace, fig_ps, ax1


    
def add_data(filename, ax1, label='', color='r'):

    data = load(filename)
    V, DV = find_and_analyze_spikes(data)

    for v, dv in zip(V, DV):
        ax1.plot(v, dv, '-', color=color, lw=.5)



if __name__=='__main__':
    import argparse
    parser=argparse.ArgumentParser(description=
     """ 
     Analysis of the Voltage Clamp protocols to determine the quality
     of the cellular access and the membrane properties
     """
    ,formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("FILES", nargs='*',\
      help="FILE(S) to be included in the analysis ",
      default=['/media/yann/DATA_EqAlain/files/DATA/2015_7_2/17_12_38_VC_MEMBRANETEST1.DAT'])
    parser.add_argument("--LABELS", "-l", nargs='*',\
      help="LABEL(S) for each of the FILE(S) included in the analysis ",
      default=[''])

    parser.add_argument("--peak_window", nargs=2,\
                        help="fraction of the peak considered for the fit (in percentage) \n"+\
                        'default= [0.1, 0.95]',
                        default=[0.1, 0.95])

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("-s", "--SAVE", help="save figures on Desktop as SVG",
                        action="store_true")

    args = parser.parse_args()
    
    if (args.LABELS==['']) or (len(args.LABELS)!=len(args.FILES)):
        args.LABELS = ['' for i in range(len(args.FILES))]
        if args.verbose:
            print 'no labels, or wrong label number'

    COLORS = np.array(['k', 'r', 'b', 'g', 'c', 'y'])[np.arange(len(args.FILES))]

    # we create the first plot
    fig_trace, fig_ps, ax1 = plot(args.FILES[0], label=args.LABELS[0])
    
    # then we add the other ones
    for i in range(1, len(args.FILES)):
        add_data(args.FILES[i], ax1, color=COLORS[i])

    for i in range(len(args.FILES)):
        ax1.plot([-60],[0], '-', lw=.5, color=COLORS[i], label=args.LABELS[i])

    if (args.LABELS[0]!=''):
        ax1.legend(frameon=False, loc='upper left', prop={'size':'x-small'})

    if args.SAVE:
        fig_ps.savefig('/home/yann/Desktop/fig_ps.svg', format='svg')

    plt.show()
