import os
from PIL import Image
import numpy as np
        
# import svgutils.transform as sg
# def concatenate_to_pdf(list_of_plots):
#     fig = sg.SVGFigure("29.7cm", "21cm")
#     XX,YY = [70,550,70,550], [50,50,400,400]
#     plot_list = []
#     for F in range(len(list_of_plots)):
#         plot = list_of_plots[F].getroot()
#         plot.moveto(XX[F],YY[F],scale=0.85)
#         plot_list.append(plot)
#     fig.append(plot_list)
#     fig.save("fig.svg")
#     os.system('inkscape --export-pdf=fig.pdf fig.svg')
#     os.system('pdftk full_figures.pdf fig.pdf cat output full_figures2.pdf')
#     os.system('mv full_figures2.pdf full_figures.pdf')

def sort_list_with_time(LIST, Hours, Mins, Seconds):
    LIST2 = []
    for hh in np.unique(Hours):
        ii = np.where(Hours==hh)
        for mm in np.unique(Mins[ii]):
            jj = np.where(Mins[ii]==mm)
            for ss in np.unique(Seconds[ii][jj]):
                kk = np.where(Seconds[ii][jj]==ss)
                for fn in LIST[ii][jj][kk]:
                    LIST2.append(fn)
    return LIST2

def get_list_of_datafile():
    full_list = sorted(os.listdir('.')) # need to sort them
    DAT_list, Hours, Mins, Seconds = [], [], [], []
    for fn in full_list:
        if os.path.isfile(fn):
            strings = fn.split('.')
            if strings[-1]=='DAT':
                DAT_list.append(fn)
                ss = fn.split('_')
                Hours.append(np.int(ss[0]))
                Mins.append(np.int(ss[1]))
                Seconds.append(np.int(ss[2]))
    DAT_list, Hours = np.array(DAT_list), np.array(Hours)
    Mins, Seconds = np.array(Mins), np.array(Seconds)
    # then we sort it in the right way !
    return sort_list_with_time(DAT_list, Hours, Mins, Seconds)

def get_list_of_png():
    full_list = sorted(os.listdir('.')) # need to sort them
    PNG_list, Hours, Mins, Seconds = [], [], [], []
    for fn in full_list:
        if os.path.isfile(fn):
            strings = fn.split('.')
            if strings[min([len(strings)-1,1])]=='png':
                PNG_list.append(fn)
                ss = fn.split('_')
                Hours.append(np.int(ss[0]))
                Mins.append(np.int(ss[1]))
                Seconds.append(np.int(ss[2]))
    PNG_list, Hours = np.array(PNG_list), np.array(Hours)
    Mins, Seconds = np.array(Mins), np.array(Seconds)
    return sort_list_with_time(PNG_list, Hours, Mins, Seconds)


def pngs_to_one_pdf_page(images, RESOLUTION=300, figure_proportion=5./7.):
    """
    images is a list of strings ['file1.png', 'files2.png', ...]
    is has to be of size 4 maximum !!! (to fit on a pdf page)
    this function creates a file called 'page.pdf'
    --------------- options 
    RESOLUTION =300 Dot Per Inches
    figure_proportion = 5./7. height over width, set in matplotlib.figure.figsize
    """
    if len(images)>15:
        print "Error, images should be of size 4 maximum"
    # A4 at resolution
    width, height = int(8.27 *RESOLUTION), int(11.7 *RESOLUTION) 
    # IMAGESIZE = int(4.5*figure_proportion*RESOLUTION), int(4.5*RESOLUTION)
    fig_width = 2.3
    fig_height = fig_width*figure_proportion
    IMAGESIZE = int(fig_width*RESOLUTION),\
       int(fig_width*figure_proportion*RESOLUTION)
    x0 = 0.5
    x1, x2 = x0+fig_width+.1,x0+2*fig_width+2*.1  # increasing
    y0 = .8  # decreasing
    y1, y2, y3, y4 = y0+fig_height+.2,y0+2*fig_height+2*.2,\
                  y0+3*fig_height+3*.2, y0+4*fig_height+4*.2
    coords = [\
              [int(x0*RESOLUTION), int(y0*RESOLUTION)],
              [int(x1*RESOLUTION), int(y0*RESOLUTION)],
              [int(x2*RESOLUTION), int(y0*RESOLUTION)],
              [int(x0*RESOLUTION), int(y1*RESOLUTION)],
              [int(x1*RESOLUTION), int(y1*RESOLUTION)],
              [int(x2*RESOLUTION), int(y1*RESOLUTION)],
              [int(x0*RESOLUTION), int(y2*RESOLUTION)],
              [int(x1*RESOLUTION), int(y2*RESOLUTION)],
              [int(x2*RESOLUTION), int(y2*RESOLUTION)],
              [int(x0*RESOLUTION), int(y3*RESOLUTION)],
              [int(x1*RESOLUTION), int(y3*RESOLUTION)],
              [int(x2*RESOLUTION), int(y3*RESOLUTION)],
              [int(x0*RESOLUTION), int(y4*RESOLUTION)],
              [int(x1*RESOLUTION), int(y4*RESOLUTION)],
              [int(x2*RESOLUTION), int(y4*RESOLUTION)]]
    page = Image.new('RGB', (width, height), 'white')
    for i in range(len(images)):
        # im = Image.open(images[i]).rotate(90)
        im = Image.open(images[i])
        im = im.resize((IMAGESIZE))  
        page.paste(im, box=(coords[i][0],coords[i][1]))
    page.save('page.pdf')
    
def loop_over_pngs_and_generate_pdf(RESOLUTION=300,figure_proportion=5/7.):
    """
    loop over all the png files in a folder and create a multipages
    pdf that plot them 12 by 12 !!
    """
    list_of_figs = []
    first_one = True
    PNG_list = get_list_of_png()
    for fn in PNG_list:
        list_of_figs.append(fn)
        if len(list_of_figs)==15:
            pngs_to_one_pdf_page(list_of_figs)
            if first_one:
                os.system('mv page.pdf full_figures.pdf')
                first_one=False
            else:
                os.system('pdftk full_figures.pdf page.pdf cat output full_figures2.pdf')
                os.system('mv full_figures2.pdf full_figures.pdf')
            list_of_figs = []
    if len(list_of_figs)>0: # need to plot the last figures
        pngs_to_one_pdf_page(list_of_figs)
        if not first_one:
            os.system('pdftk full_figures.pdf page.pdf cat output full_figures2.pdf')
            os.system('mv full_figures2.pdf full_figures.pdf')
            # now let's clean up a bit
            os.system('rm page.pdf')
        else:
            os.system('mv page.pdf full_figures.pdf')
    print "the file :"
    print " ---- full_figures.pdf ------- "
    print " has been generated succesfully"

    
#####################################################################
### AUTOMATICALLY LOOP OVER FILES AND MAKE FIGURES TO A PDF FILE ####
#####################################################################

import matplotlib.pylab as plt
import elphy_to_python as ep
import classic_electrophy
import electrophy as el
import tf_analysis

def analyze_and_save_png(filename, command_channel=2):
        exp, time = ep.get_experiment_name_and_time(filename)
        if exp.split('_')[0]=='TF':
            l_fig = tf_analysis.analysis(filename,\
                    command_channel=command_channel) # returns list of fig
        else:
            l_fig = classic_electrophy.analysis(filename,\
                    command_channel=command_channel) # returns list of fig
        nn=1
        for fig in l_fig:
            fig_name = 'figs/'+filename.replace('1.DAT',str(nn)+'.png')
            try:
                fig.savefig(fig_name, dpi=200)
            except ValueError:
                fig.savefig(fig_name)
            nn+=1

    
def full_day_analysis(index_to_start=0, index_to_stop=10000,\
                      command_channel=1):
    """
    use of index in case of memory problems !!!
    """
    if not os.path.isdir('figs'):
        os.system('mkdir figs')
    list_of_DAT = get_list_of_datafile()
    ii = index_to_start
    if index_to_stop>len(list_of_DAT):
        index_to_stop=len(list_of_DAT)
    while ii<index_to_stop:
        fn = list_of_DAT[ii]
        print 'o=> index of file :', ii
        analyze_and_save_png(fn, command_channel=command_channel)
        plt.close('all')
        ii+=1
    print 'step :', ii,' over :', len(list_of_DAT)

