#!/usr/bin/python
# Filename: tf.py

import numpy as np
from scipy import integrate

def rudolph_rho_v(V,fe,fi,params):
    """
    args : v,params
    returns : the distribution for the membrane potential as the one derived
    in Rudolph et al. 2003 for multiplicative noise
    !!! IT NEEDS TO BE RENORMALIZED !!!
    """
    Cm = params['Cm'];Gl=params['Gl'];El=params['El']
    Qe = params['Qe'];Ne = params['Ne'];Te=params['Te'];Ee=params['Ee']
    Qi = params['Qi'];Ni = params['Ni'];Ti=params['Ti'];Ei=params['Ei']

    np.where(fe<=0,fe,1e-4);np.where(fi<=0,fi,1e-4)# security in the calculus (see A2)
    ge=fe*Ne*Qe*Te
    sge=Qe*np.sqrt(fe*Ne*Te/2)
    gi=fi*Ni*Qi*Ti
    sgi=Qi*np.sqrt(fi*Ni*Ti/2)

    A1 = - (2*Cm*(ge+gi)+2*Cm*Gl+sge**2*Te+sgi**2*Ti)
    A1 /= 2*(sge**2*Te+sgi**2*Ti)
    A2 = 2*Cm*(Gl*(sge**2*Te*(El-Ee)+sgi**2*Ti*(El-Ei))+\
               (ge*sgi**2*Ti-gi*sge**2*Te)*(Ee-Ei))
    A2 /= (Ee-Ei)*np.sqrt(sge**2*Te*sgi**2*Ti)*(sge**2*Te+sgi**2*Ti)
    a_ln = sge**2*Te/Cm*(V-Ee)**2+sgi**2*Ti/Cm*(V-Ei)**2
    a_arctan = sge**2*Te*(V-Ee)+sgi**2*Ti*(V-Ei)
    a_arctan /= (Ee-Ei)*np.sqrt(sge**2*Te*sgi**2*Ti)
    return np.exp(A1*np.log(a_ln)+A2*np.arctan(a_arctan))

def rudolph_rho_normed(V,fe,fi,params):
    integrand = lambda x: rudolph_rho_v(x,fe,fi,params)
    N,error = integrate.quad(integrand,-np.inf,np.inf) # normalization factor
    print "absolute error on the function :",error
    return rudolph_rho_v(V,fe,fi,params)/N

def proba_rudolph(fe,fi,params):
    threshold = params['Vthre']
    # version with analytic integration
    integrand = lambda x: rudolph_rho_v(x,fe,fi,params)
    N,error = integrate.quad(integrand,-np.inf,np.inf) # normalization factor
    return integrate.quad(integrand,threshold,np.inf)[0]/N
    """
    # version with discrete integration
    vv = np.linspace(threshold,0,1e2)
    distr = rudolph_rho_normed(vv,fe,fi,params)
    return integrate.cumtrapz(distr,vv)[0]
    """

def tau_eff(fe,fi,params):
    """
    args : params
    returns : the efective membrane time constant
    """
    Cm = params['Cm'];Gl=params['Gl'];El=params['El']
    Qe = params['Qe'];Ne = params['Ne'];Te=params['Te'];Ee=params['Ee']
    Qi = params['Qi'];Ni = params['Ni'];Ti=params['Ti'];Ei=params['Ei']

    ge=fe*Ne*Qe*Te
    gi=fi*Ni*Qi*Ti
    return Cm/(Gl+ge+gi)

def kuhn_func(fe,fi,params):
    return proba_rudolph(fe,fi,params)/tau_eff(fe,fi,params)
    
    
        

