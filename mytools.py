#!/usr/bin/python
# Filename: mytools.py

import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg.fblas import dgemm
import sys

def print_Dictionnary(DICT):
    for item in DICT:
        print item + ' = ' + str(DICT[item])

        
def linear_least_squares_1D(x, y, residuals=False):
    """
    Return the least-squares solution to a linear matrix equation.
    Solves the equation `a x = b` by computing a vector `x` that
    minimizes the Euclidean 2-norm `|| b - a x ||^2`. The equation may
    be under-, well-, or over- determined (i.e., the number of
    linearly independent rows of `a` can be less than, equal to, or
    greater than its number of linearly independent columns). If `a`
    is square and of full rank, then `x` (but for round-off error) is
    the "exact" solution of the equation.
    Parameters
    ----------
    a : (M, N) array_like
    "Coefficient" matrix.
    b : (M,) array_like
    Ordinate or "dependent variable" values.
    residuals : bool
    Compute the residuals associated with the least-squares solution
    Returns
    -------
    x : (M,) ndarray
    Least-squares solution. The shape of `x` depends on the shape of
    `b`.
    residuals : int (Optional)
    Sums of residuals; squared Euclidean 2-norm for each column in
    ``b - a*x``.

    EXAMPLE:
    import matplotlib.pyplot as plt
    x = np.arange(20)
    y = x+np.random.randn(len(x))*3
    c0, c1 = linear_least_squares_1D(x, y)
    plt.plot(x, y, 'o', label='Original data', markersize=10)
    plt.plot(x, c0 * x + c1, 'r', label='Fitted line')
    plt.legend(loc='best')
    plt.show()
    """
    a = np.vstack([x, np.ones(len(x))]).T
    a = np.asarray(a, order='c')
    
    if type(a) != np.ndarray or not a.flags['C_CONTIGUOUS']:
        warn('Matrix a is not a C-contiguous numpy array. The solver will create a copy, which will result' + \
             ' in increased memory usage.')

    a = np.asarray(a, order='c')
    i = dgemm(alpha=1.0, a=a.T, b=a.T, trans_b=True)
    C = np.linalg.solve(i, dgemm(alpha=1.0, a=a.T, b=y)).flatten()
    Res = np.linalg.norm(np.dot(a, C) - y)
    
    if residuals:
        return C, Res
    else:
        return C
        


if __name__=='__main__':
    if sys.argv[-1]=='LSQ':
        x = np.arange(20)
        y = x+np.random.randn(len(x))*3
        c0, c1 = linear_least_squares_1D(x, y)
        plt.plot(x, y, 'o', label='Original data', markersize=10)
        plt.plot(x, c0 * x + c1, 'r', label='Fitted line')
        plt.legend(loc='best')
        plt.show()
    
