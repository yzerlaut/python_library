#!/usr/bin/python
# Filename: impedance.py
import numpy as np
import matplotlib.pylab as plt
from scipy.integrate import cumtrapz
import signanalysis as sgna
import fourier as fourier


def psd_and_phase_shift(v, t, i, max_freq=1e3, min_freq=0.05, normalize=False):
    """
    return th 
    """
    freq_v,psd_v,phase_v = fourier.real(v,t[1])
    freq_i,psd_i,phase_i = fourier.real(i,t[1])

    lim = int(max_freq/freq_v[1]) # until max_freq restriction 
    start = int(min_freq/freq_v[1])+1 # from min_freq

    Imped = np.sqrt(psd_v[start:lim]/psd_i[start:lim])
    # if normalize:
    #     Imped = Imped/Imped.max()
    freq_v = freq_v[start:lim]
    phase_shift = (phase_v[start:lim]-phase_i[start:lim])%(2*np.pi)
    return freq_v, Imped, phase_shift


def impedance_plot(t,v,i,details=[[0,1],[2,3]],title='',\
                   max_freq=1e4,min_freq=0.05,phase=True,\
                   normalize=False):
    """
    this function calculates and plot the impedance spectrum
    of a physical system, it also calculates and show the slope
    of the impedance in the log plot in the desired intervals,
    e.g. details = [[0,1],[2.5,3.4],[3.45,3.6]]
    
    argument : t=np.array(),v=np.array(),i=np.array()
    optional arguments : min_freq,max_freq,details = [],title=''
    
    by default details=[[0,1],[2,3]]
    you can put a string for the suptitle
    
    returns the plot
    if phase=True (default) it also plots the phase
    """
    
    freq_v, Imped, phase_shift = psd_and_phase_shift(v, t, i,\
                    max_freq=max_freq, min_freq=min_freq,\
                    normalize=normalize)


    log_freq = np.log(freq_v)/np.log(10)
    log_imped = np.log(Imped)/np.log(10)

    log_imped_fit,log_freq_fit,poly = \
      sgna.poly_smooth(log_imped,log_freq,return_polynom=True)


    fig = plt.figure(figsize=(8,6))
    ax1 = plt.subplot(111)
    ax1.set_title(title)
    ax1.plot(log_freq,log_imped,alpha=.3)
    lim_plot = 5
    ax1.plot(log_freq_fit[lim_plot:-lim_plot],log_imped_fit[lim_plot:-lim_plot],'b--',linewidth=4, label='psd')
    ax1.legend(loc='lower left', frameon=False)
    for i in range(len(details)):
        w1 = details[i][0]
        w2 = details[i][1]
        # then domain by domain, we get the slope in the interval given by details
        w = np.linspace(w1,w2,10)
        p = np.polyfit(w,np.polyval(poly,w),1) # we look for the linear 
        ax1.plot(w,np.polyval(p,w),'k-',linewidth=.7)
        ax1.annotate('slope \n'+str(round(p[0],2)),(w1,-1.5))
    ax1.set_xlabel('frequency, log$_{10}$(Hz)')
    ax1.set_ylabel('log$_{10}$(Z)')
    if phase:
        smooth_phase,log_freq_fit,poly = sgna.poly_smooth(phase_shift,log_freq,return_polynom=True)
        ax2 = plt.twinx()
        ax2.plot(log_freq, phase_shift, alpha=.1, color='r')
        ax2.plot(log_freq_fit[lim_plot:-lim_plot],smooth_phase[lim_plot:-lim_plot],'r--',linewidth=2, label='phase')
        ax2.legend(loc='upper left', frameon=False)
        ax2.set_ylabel('phase shift (Rd)')
        plt.yticks([0, np.pi, 2*np.pi], ['0','$\pi$', '2$\pi$'])
    plt.tight_layout()
    return fig
