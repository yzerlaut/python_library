"""
splits an avi videos into its frames as png images
"""
import os, sys
from PIL import Image

def split_avi_to_pngs(avi_file, png_folder='./'):
    root_filename = avi_file.replace('.avi', '')
    os.system("avconv -i "+avi_file+" -f image2 "+\
              png_folder+root_filename+"%00d.png")

if __name__=='__main__':
    split_avi_to_pngs(sys.argv[-1], png_folder='./pngs/')
