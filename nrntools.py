#!/usr/bin/python
# coding=utf-8

from neuron import h as nrn
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, collections, transforms, colors, ticker

def load_model(hoc_name, dll_name=None):
    # we start by deleting all the rest
    for SEC in nrn.allsec():
        nrn.delete_section(sec=SEC)
    if dll_name:
        nrn.nrn_load_dll(dll_name)
    nrn.load_file(hoc_name)


def integrate(tstop):
    V_all = []
    while h.t < tstop:
        h.fadvance()
        v= get_i()
        V_all.append(v)
    t = np.arange(0, len(V_all))*h.dt
    return t, np.array(V_all)

def insert_extracellular():
    for sec in h.allsec():
        sec.insert("extracellular")

def get_v():
    v = []
    for sec in h.allsec():
        for seg in sec:
            v.append(seg.v)
    return v

def get_i():
    v = []
    for sec in h.allsec():
        for seg in sec:
            v.append(seg.i_membrane)
    return v

def get_nsegs():
    nsegs = 0
    for sec in nrn.allsec():
        nsegs += sec.nseg
    return nsegs


def get_coords():
    total_segs = get_nsegs()
    coords = np.zeros(total_segs,
                      dtype=[("x", np.float32),
                             ("y", np.float32),
                             ("z", np.float32),
                             ("L", np.float32),
                             ("diam", np.float32)
                            ])
    j = 0
    for sec in nrn.allsec():
        n3d = int(nrn.n3d(sec))
        x = np.array([nrn.x3d(i,sec) for i in range(n3d)])
        y = np.array([nrn.y3d(i,sec) for i in range(n3d)])
        z = np.array([nrn.z3d(i,sec) for i in range(n3d)])
        nseg = sec.nseg
        pt3d_x = np.arange(n3d)
        seg_x = np.arange(nseg)+0.5

        if len(pt3d_x)<1:
            x_coord = y_coord = z_coord =np.ones(nseg)*np.nan
        else:
            x_coord = np.interp(seg_x, pt3d_x, x)
            y_coord = np.interp(seg_x, pt3d_x, y)
            z_coord = np.interp(seg_x, pt3d_x, z)
      
        lengths = np.zeros(nseg)
        diams = np.zeros(nseg)
        
        lengths = np.ones(nseg)*sec.L*1./nseg
        diams = np.ones(nseg)*sec.diam

        coords['x'][j:j+nseg]=x_coord
        coords['y'][j:j+nseg]=y_coord
        coords['z'][j:j+nseg]=z_coord
        coords['L'][j:j+nseg]=lengths
        coords['diam'][j:j+nseg]=diams

        j+=nseg


    return coords

def get_seg_coords():
    nchars = 12
    total_segs = get_nsegs()
    coords = np.zeros(total_segs,
                      dtype=[("x0", np.float32),
                             ("y0", np.float32),
                             ("z0", np.float32),
                             ("x1", np.float32),
                             ("y1", np.float32),
                             ("z1", np.float32),
                             ("L", np.float32),
                             ("diam", np.float32),
                             ("name", "|S%d" % nchars)
                            ])
    j = 0
    for sec in nrn.allsec():
        n3d = int(nrn.n3d(sec))
        x = np.array([nrn.x3d(i,sec) for i in range(n3d)])
        y = np.array([nrn.y3d(i,sec) for i in range(n3d)])
        z = np.array([nrn.z3d(i,sec) for i in range(n3d)])

        arcl = np.sqrt(np.diff(x)**2+np.diff(y)**2+np.diff(z)**2)
        arcl = np.cumsum(np.concatenate(([0], arcl)))
        nseg = sec.nseg
        pt3d_x = arcl/arcl[-1]
        
        diams = np.ones(nseg)*sec.diam
        lengths = np.ones(nseg)*sec.L*1./nseg
        names = np.repeat(sec.name()[:nchars], nseg).astype("|S%d"%nchars)

        seg_x = np.arange(nseg)*1./nseg
        
        x_coord = np.interp(seg_x, pt3d_x, x)
        y_coord = np.interp(seg_x, pt3d_x, y)
        z_coord = np.interp(seg_x, pt3d_x, z)
      

        coords['x0'][j:j+nseg]=x_coord
        coords['y0'][j:j+nseg]=y_coord
        coords['z0'][j:j+nseg]=z_coord
        
        seg_x = (np.arange(nseg)+1.)/nseg

        x_coord = np.interp(seg_x, pt3d_x, x)
        y_coord = np.interp(seg_x, pt3d_x, y)
        z_coord = np.interp(seg_x, pt3d_x, z)

        coords['x1'][j:j+nseg]=x_coord
        coords['y1'][j:j+nseg]=y_coord
        coords['z1'][j:j+nseg]=z_coord

        coords['diam'][j:j+nseg] = diams
        coords['L'][j:j+nseg] = lengths
        coords['name'][j:j+nseg] = names 
        j+=nseg

    return coords
    
def plot_neuron(coords,scalar=None, colors=None,
                norm=colors.Normalize(), cmap=cm.jet):
   
    a = plt.gca()

    line_segs = [[(c['x0'], c['z0']), (c['x1'], c['z1'])] for c in coords]

    col = collections.LineCollection(line_segs, cmap=cmap, norm=norm)
    a.add_collection(col, autolim=True)
    if scalar is not None:
        col.set_array(scalar)
    else:
        col.set_color(colors)

    a.autoscale_view()
    plt.axis('equal')
    return col


def get_extended_coords(xAxis='x',yAxis='y',zAxis='z'):
    nchars = 12
    total_segs = get_nsegs()
    coords = np.zeros(total_segs,
                      dtype=[("x0", np.float32),
                             ("y0", np.float32),
                             ("z0", np.float32),
                             ("x1", np.float32),
                             ("y1", np.float32),
                             ("z1", np.float32),
                             ("L", np.float32),
                             ("diam", np.float32),
                             ("name", "|S%d" % nchars),
                             ("parent", "|S%d" % nchars)
                            ])
    j = 0
    for SEC in nrn.allsec():
        n3d = int(nrn.n3d(SEC))
        x = np.array([nrn.x3d(i,SEC) for i in range(n3d)])
        y = np.array([nrn.y3d(i,SEC) for i in range(n3d)])
        z = np.array([nrn.z3d(i,SEC) for i in range(n3d)])

        arcl = np.sqrt(np.diff(x)**2+np.diff(y)**2+np.diff(z)**2)
        arcl = np.cumsum(np.concatenate(([0], arcl)))
        nseg = SEC.nseg
        pt3d_x = arcl/arcl[-1]
        
        diams = np.ones(nseg)*SEC.diam
        lengths = np.ones(nseg)*SEC.L*1./nseg
        names = np.repeat(SEC.name()[:nchars], nseg).astype("|S%d"%nchars)

        seg_x = np.arange(nseg)*1./nseg
        
        x_coord = np.interp(seg_x, pt3d_x, x)
        y_coord = np.interp(seg_x, pt3d_x, y)
        z_coord = np.interp(seg_x, pt3d_x, z)
      

        coords[xAxis+'0'][j:j+nseg]=x_coord
        coords[yAxis+'0'][j:j+nseg]=y_coord
        coords[zAxis+'0'][j:j+nseg]=z_coord
        
        seg_x = (np.arange(nseg)+1.)/nseg

        x_coord = np.interp(seg_x, pt3d_x, x)
        y_coord = np.interp(seg_x, pt3d_x, y)
        z_coord = np.interp(seg_x, pt3d_x, z)
        
        
        coords[xAxis+'1'][j:j+nseg]=x_coord
        coords[yAxis+'1'][j:j+nseg]=y_coord
        coords[zAxis+'1'][j:j+nseg]=z_coord

        coords['diam'][j:j+nseg] = diams
        coords['L'][j:j+nseg] = lengths
        coords['name'][j:j+nseg] = names 

        REF = nrn.SectionRef(sec=SEC)
        if REF.has_parent()==1:
            names = np.repeat(REF.parent.name()[:nchars],\
                              nseg).astype("|S%d"%nchars)
        else:
            names = np.repeat('XX'[:nchars],\
                              nseg).astype("|S%d"%nchars)
        coords['parent'][j:j+nseg]=names
        j+=nseg
    return coords


# make a dictionnaory of the branching
def BranchScan(coords):
    branching = []
    for mother in range(coords.size):
        childs = []
        childs.append(coords[mother]['name']) # the first element of branching is the mother 
        childs.append(mother) # + we return the coords indice for future use
        for child in range(coords.size):
            if coords[child]['parent']==coords[mother]['name']:
                childs.append(coords[child]['name'])
                childs.append(child)
        branching.append(childs)
    return branching
    

def norm(vec):
    return np.sqrt(vec[0]**2+vec[1]**2+vec[2]**2)

def colatitude(vec):
    if norm(vec)!=0:
        col = np.arccos(vec[2]/norm(vec))
    else:
        col=0
    return col

def longitude(vec):
    if norm(vec)==0:
        print " vecteur nul "
    if vec[1]>=0: # if y>=0
        longit = np.arccos(vec[0]/(vec[0]**2+vec[1]**2))
    else:
        longit = 2*np.pi - np.arccos(vec[0]/(vec[0]**2+vec[1]**2))
    return longit
        

def CalcBranchCoeff(branching,coords):
    PlusBranch = []
    MinusBranch = []
    BissecAngle = []
    
    for branch in branching: # weloop over the branching points
    
        if len(branch)==6: # to begin, iwe have just a tetraedre
        # mother is connected in 1, childs in 0
        # the vectors are sempre from 0 to 1 !!!

            im = branch[1] # indice of the mother segment
            i1 = branch[3] #indice of the 1st segment
            i2 = branch[5]
            
        
            mother_vec = np.array([ coords[im]['x1']-coords[im]['x0'],\
                                    coords[im]['y1']-coords[im]['y0'],\
                    coords[im]['z1']-coords[im]['z0'] ] )
            child1_vec = np.array([ coords[i1]['x1']-coords[i1]['x0'],\
                    coords[i1]['y1']-coords[i1]['y0'],\
                    coords[i1]['z1']-coords[i1]['z0'] ] )
            child2_vec = np.array([ coords[i2]['x1']-coords[i2]['x0'],\
                    coords[i2]['y1']-coords[i2]['y0'],\
                    coords[i2]['z1']-coords[i2]['z0'] ] )

            lm = longitude(mother_vec)
            l1 = longitude(child1_vec)
            l2 = longitude(child2_vec)
            
            bissec_12 = child2_vec + child1_vec
            bissec_1m = child1_vec - mother_vec
            bissec_2m = child2_vec - mother_vec
        
            #ThreeBissec_projec = [bissec_12[2]/norm(bissec_12),bissec_1m[2]/norm(bissec_1m),bissec_2m[2]/norm(bissec_2m)]
            ThreeBissec_projec = [np.cos((l1+l2)/2),np.cos((l1+lm)/2),np.cos((lm+l2)/2)]
            SizeFactor = coords[im]['L']*coords[i1]['L']*coords[i2]['L']*coords[im]['diam']*\
                            coords[i1]['diam']*coords[i2]['diam']
            
            plusbranch = max(ThreeBissec_projec)*SizeFactor
            minbranch = min(ThreeBissec_projec)*SizeFactor
        
            PlusBranch.append( [ coords[im]['x1'],coords[im]['y1'],coords[im]['z1'],plusbranch,coords[im]['name'] ])
            MinusBranch.append( [ coords[im]['x1'],coords[im]['y1'],coords[im]['z1'],minbranch,coords[im]['name'] ])
            BissecAngle.append(ThreeBissec_projec)
            
    return PlusBranch,MinusBranch,BissecAngle

