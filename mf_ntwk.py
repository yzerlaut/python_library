#!/usr/bin/python
# Filename: ntwk_dyn.py

import numpy as np
import matplotlib.pylab as plt
import numpy.fft as fft

def simple_syst(X):
    """
    arg : take the set of variables (nu,cov1,cov2,cov3,corr)
    -> returns the time derivative
    (Dnu,Dcov1,Dcov2,Dcov3,Dcorr)

    \\ !! A are the fit params, it has to be set GLOBAL !! //
    """

    # first moment
    dS0 = -X[0]+mu(X[0],X[0],A)
    return np.array([dS0])


def sami_syst(X):
    """
    arg : take the set of variables (nu,cov1,cov2,cov3,corr)
    -> returns the time derivative
    (Dnu,Dcov1,Dcov2,Dcov3,Dcorr)

    \\ !! A are the fit params, it has to be set GLOBAL !! //
    """

    # first moment
    dS0 = -X[0]+mu(X[0],X[1],A)+X[2]*diff2_me_me(X[0],X[1],A)+\
        2*X[3]*diff2_me_mi(X[0],X[1],A)+X[4]*diff2_mi_mi(X[0],X[1],A)

    dS1 = -X[1]+mu(X[0],X[1],A)+ X[2]*diff2_me_me(X[0],X[1],A)+\
        2*X[3]*diff2_me_mi(X[0],X[1],A)+X[4]*diff2_mi_mi(X[0],X[1],A)
    
    # covariances
    dS2 = -2*X[2]+ mu(X[0],X[1],A)*(1/T-mu(X[0],X[1],A)) + \
        (mu(X[0],X[1],A)-X[0])^2 + \
        2*diff_me(X[0],X[1],A)*X[2]+2*diff_mi(X[0],X[1],A)*X[3]
    
    dS3 = (mu(X[0],X[1],A)-X[0])*(mu(X[0],X[1],A)-X[1]) - 2*X[3] +\
        diff_me(X[0],X[1],A)*(X[2]+X[3])+diff_mi(X[0],X[1],A)*(X[3]+X[4])
     
    dS4 = -2*X[4]+ mu(X[0],X[1],A)*(1/T-mu(X[0],X[1],A)) + \
        (mu(X[0],X[1],A)-X[1])^2 +\
        2*diff_mi(X[0],X[1],A)*X[4]+2*diff_me(X[0],X[1],A)*X[3]

    return np.array([dS0,dS1,dS2,dS3,dS4])
