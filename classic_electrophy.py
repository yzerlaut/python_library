import numpy as np
np.seterr(over='ignore') # for the VC memb test fit
import matplotlib.pylab as plt
import elphy_to_python as ep
import matplotlib

# new modulus
import electrophy

from scipy.optimize import curve_fit, minimize
import os

FIGSIZE = (7,5) # global figsize, common to all generated figures

###### ------- SOME GENERAL FUNCTIONS ----------- ######

def find_crossing_time(t, v, crossing=-40):
    spike_indexes = np.where(np.sign(v[:-1]-crossing)<np.sign(v[1:]-crossing))
    return t[spike_indexes]


###############################################################
###### ------- ANALYSIS PROTOCOL BY PROTOCOL ----------- ######
###############################################################

###### --- Membrane Test in Voltage clamp (to estimate Rs, Rm and Cm)

def VC_membrane_test(filename, t_pre=2, t_post=13, return_fig=False,
                     fraction_of_peak_for_Time_cst=99./100.,
                     return_with_infos=False,\
                     command_channel=2):
    """
    t_pre and t_post set the windows of sampling of the stationary
    currents :
    Ibase = I[t2-t_pre,t2], Iend=[t3-t_post,t3]
    they are also usefull for the visualization
    we visualize : [t2-t_pre, t3-t_post]
    """
    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    t, data = ep.get_signals_episode(filename)
    params = ep.get_metadata(filename) # metadata
    dt = t[1]-t[0]

    if params.has_key('t4'): # if everything recorded !
        t1, t2, t3, t4 = float(params['t1']), float(params['t2']),\
             float(params['t3']), float(params['t4'])
        amp = float(params['pulse_amplitude'])
    else:
        # default values
        t1, t2, t3, t4 = 20, 40 , 60, 80
        amp = -5
    
    if return_fig:
        fig = plt.figure(figsize=FIGSIZE)
        plt.subplots_adjust(bottom=.13, right=.97, left=.135)
        plt.suptitle('Membrane Test : '+time)
        p1 = plt.subplot(111) # main figure
        sp1 = plt.axes([0.58,0.34,0.3,0.3], frameon=False) # inset for full trace
      
    mean_i_response = np.zeros(t.size)
    for i in range(len(data)):
        if return_fig:
            sp1.plot(t, data[i][0], color='b', lw=0.5)
        mean_i_response += data[i][0]/(len(data))
    xstim = np.array([10,t1,t1,t2,t2,t3,t3,t4,t4,t4+t1-10])
    ystim = np.array([0,0,-amp/2.,-amp/2.,amp/2.,amp/2.,-amp/2.,-amp/2.,0,0])
    ystim = ystim/(ystim.max()-ystim.min())*0.3*(mean_i_response.max()-mean_i_response.min())
    ystim += mean_i_response.min()-0.3*(mean_i_response.max()-mean_i_response.min())
    i_interest = mean_i_response[int((t2-t_pre)/dt):int((t3-t_post)/dt)]
    t_interest = t[int((t2-t_pre)/dt):int((t3-t_post)/dt)]
    # calculus of the leak current
    leak = mean_i_response[:int(t1/dt)-1].mean()
    # now calculus of all the membrane parameters
    Ibase = mean_i_response[int((t2-t_pre)/dt):int((t2)/dt)].mean()
    Iend = mean_i_response[int((t3-t_post)/dt):int((t3)/dt)].mean()
    DI_stat = Iend-Ibase
    Ijump = i_interest.min()-Ibase
    # i_min = np.argmin(i_interest)
    ## multiplt possibilites here, we take 2/3 of the min amplitude by default
    i_min = np.argmin(abs(i_interest-Iend-(i_interest.min()-Iend)*fraction_of_peak_for_Time_cst))
    # curve fitting
    t_fit, i_fit = t_interest[i_min:], i_interest[i_min:]
    t_fit = t_fit-t_fit[0] # to start from zero !
    i_fit = (i_fit-i_fit.min())/(i_fit.max()-i_fit.min()) # normalized from 0 to 1 !
    def func(x, a): # will take a normalized current to have less parameters
        return 1-np.exp(-a*x)
    y = func(t_fit, 1) # guess
    success_flag=False
    try:
        popt, pcov = curve_fit(func, t_fit, i_fit)
        if popt[0]!=0:
            Tau=1./popt[0]
        else:
            Tau=1e9
        success_flag=True
        no_norm_i_fit = i_interest[i_min:].min()+func(t_fit, popt[0])*(i_interest[i_min:].max()-i_interest[i_min:].min())
    except RuntimeError:
        print "Fit for the Membrane Test not achieved !"
        Tau=1e9
    # now that we have Tau, we can do the calculus
    Tintegration = t3-t_post-t2-3*dt # we remove the 2 first steps
    integral_response = \
      np.trapz(mean_i_response[int(t2/dt)+2:int((t3-t_post)/dt)], dx=dt)\
      -Ibase*Tintegration
    integral_resist = DI_stat*(Tintegration+Tau*(np.exp(-Tintegration/Tau)-1))
    integral_capa = integral_response-integral_resist
    if integral_capa!=0:
        Rs=Tau*amp/integral_capa*1000.0  # ms*mV / ms*pA *1000 -> Mohm
    else:
        Rs=1e6
    if DI_stat!=0:
        Rm=amp/DI_stat*1000-Rs #{ mV/pA*1000 -> Mohm  }
    else:
        Rm=1e6 
    Cm=Tau*(Rs+Rm)/(Rm*Rs)*1000 # {ms/Mohm*1000-> pF }

    if return_fig:
        sp1.plot(t, mean_i_response, color='r', lw=1)
        sp1.set_xticks([0, t1, t2, t3, t4, t4+t1])
        sp1.set_xlabel('ms')
        sp1.set_xlabel('time (ms)', fontsize=13)
        sp1.plot(xstim, ystim, 'k', lw=2)
        sp1.get_xaxis().tick_bottom()
        sp1.get_yaxis().set_visible(False)
        y0,y1 = sp1.get_yaxis().get_view_interval()
        sp1.plot([0,t4+t1],[y0,y0],'k', lw=2)
        # we plot the interesting region
        p1.plot(t_interest, i_interest, 'b', lw=2)
        p1.annotate(' Ileak:\n'+str(round(leak))+'pA', (.15,.62),\
                xycoords='figure fraction')
        if success_flag:
            p1.plot(t_fit+t_interest[i_min], no_norm_i_fit, 'k--', lw=3)
        p1.annotate('  Rs:\n'+str(round(Rs))+'M$\Omega$', (.15,.47), xycoords='figure fraction')    
        p1.annotate('  Rm:\n'+str(round(Rm))+'M$\Omega$',(.15,.32), xycoords='figure fraction')    
        p1.annotate('  Cm:\n'+str(round(Cm))+'pF', (.15,.17), xycoords='figure fraction')    
        p1.set_ylabel('current (pA)')
        p1.set_xlabel('time (ms)')
        # plt.tight_layout()
        if return_with_infos:
            return [fig, Rs, leak]
        else:
            return [fig]
            
    else:
        if success_flag:
            return t_interest, i_interest, t_fit+t_interest[i_min], no_norm_i_fit, [Rs,Rm,Cm]
        else:
            return t_interest, i_interest



###### --- Membrane Test in Currrent clamp (to estimate Rm and Cm)
### usually after Rs compensation

### --- SINGLE COMPARTMENT APPROXIMATION
def make_fit_for_single_comp(t, v_trace, DiA, t0, maxiter=10000,\
                        initial_value=(-70.,5.,1./30.)):
    """ units: ms, mV, pA, """
    H = np.array([0. if tt<t0 else 1. for tt in t]) # heaviside function
    def step_response_1_comp(A, B, C):
        return A+B*(1-np.exp(-C*(t-t0)))*H
    def to_minimize(P):
        return np.mean(np.abs(v_trace-step_response_1_comp(*P)))
    bounds = ((-100., -40.), (-40.,40), (1./5e2,1.))
    res = minimize(to_minimize, initial_value,\
                 method='L-BFGS-B', bounds=bounds,\
                 options={'maxiter':maxiter})
    v_fit = step_response_1_comp(*res.x)
    A, B, C = res.x
    # translated into membrane parameters (units: Mohm, mV, pF)
    Rm, El, Cm = np.round(1e3*B/DiA,1), np.round((A+B),1), np.round(DiA/B/C,1) 
    return [Rm, El, Cm], v_fit

### --- DOUBLE COMPARTMENT APPROXIMATION
def make_fit_for_double_comp(t, v_trace, DiA, t0, maxiter=100000,\
         initial_value=(-70.,5.,1./30.,5.,1./30.)):
    """ units: ms, mV, pA, """

    H = np.array([0. if tt<t0 else 1. for tt in t]) # heaviside function
    def step_response_2_comp(A, B, C, D, E):
        return A+B*(1-np.exp(-C*(t-t0)))*H+D*(1-np.exp(-E*(t-t0)))*H
    def to_minimize(P):
        return np.mean(np.abs(v_trace-step_response_2_comp(*P)))
    bounds = ((-100., -40.), (-40.,40), (1./5e2,1.), (-40.,40), (1./5e2,1.))
    res = minimize(to_minimize, initial_value,\
                 method='L-BFGS-B', bounds=bounds,\
                 options={'maxiter':maxiter})
    v_fit = step_response_2_comp(*res.x)
    A, B, C, D, E = res.x
    # translated into membrane parameters (units: Mohm, mV, pF)
    ### to be done, not trivial (need to pursue the matrix calculus) !!!
    RmS, CmS, Ra, RmD, CmD = 0, 0, 0, 0, 0
    return [RmS, CmS, Ra, RmD, CmD], v_fit


## calculus for the 2 compartment model
def func_2comp_full(t, g1, g2, ga, C1, C2, El, I0):
    # units : ms, nS, pF, mV, pA
    return (C2*((g2 + ga)/C2 - (C2*g1 + C1*g2 + C1*ga + C2*ga +  np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/(2.*C1*C2)        )*(((C2*g1 + C1*g2 + C1*ga + C2*ga -              np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))*           ((C1*El*g2*((g2 + ga)/C2 -                   (C2*g1 + C1*g2 + C1*ga + C2*ga -                      np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/                   (2.*C1*C2)))/              np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)) -              (ga*(El*g1 + I0))/              np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))))/         (2.*C1*C2*((g1*g2)/(C1*C2) + (g1*ga)/(C1*C2) + (g2*ga)/(C1*C2))) +         (-((C1*El*ga)/              np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))) +            (C1*C2*El*((g2 + ga)/C2 - (C2*g1 + C1*g2 + C1*ga + C2*ga -                    np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/                 (2.*C1*C2)))/            np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)) -            ((C2*g1 + C1*g2 + C1*ga + C2*ga -                 np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))*              ((C1*El*g2*((g2 + ga)/C2 -                      (C2*g1 + C1*g2 + C1*ga + C2*ga -                         np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 -                           4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/(2.*C1*C2)))/                 np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)) -                 (ga*(El*g1 + I0))/                 np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))))/            (2.*C1*C2*((g1*g2)/(C1*C2) + (g1*ga)/(C1*C2) + (g2*ga)/(C1*C2))))/         np.exp(((C2*g1 + C1*g2 + C1*ga + C2*ga +                 np.sqrt(C2**2*g1**2 - 2*C1*C2*g1*g2 + C1**2*g2**2 - 2*C1*C2*g1*ga + 2*C2**2*g1*ga +                   2*C1**2*g2*ga - 2*C1*C2*g2*ga + C1**2*ga**2 + 2*C1*C2*ga**2 + C2**2*ga**2))*t)/            (2.*C1*C2))))/ga + (C2*((g2 + ga)/C2 -         (C2*g1 + C1*g2 + C1*ga + C2*ga -            np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/(2.*C1*C2)        )*(((C2*g1 + C1*g2 + C1*ga + C2*ga +              np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))*           (-((C1*El*g2*((g2 + ga)/C2 -                     (C2*g1 + C1*g2 + C1*ga + C2*ga +                        np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))                       )/(2.*C1*C2)))/                np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))) +              (ga*(El*g1 + I0))/              np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))))/         (2.*C1*C2*((g1*g2)/(C1*C2) + (g1*ga)/(C1*C2) + (g2*ga)/(C1*C2))) +         ((C1*El*ga)/np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)) -            (C1*C2*El*((g2 + ga)/C2 - (C2*g1 + C1*g2 + C1*ga + C2*ga +                    np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/                 (2.*C1*C2)))/            np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)) -            ((C2*g1 + C1*g2 + C1*ga + C2*ga +                 np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga)))*              (-((C1*El*g2*((g2 + ga)/C2 -                        (C2*g1 + C1*g2 + C1*ga + C2*ga +                           np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 -                             4*C1*C2*(g1*g2 + g1*ga + g2*ga)))/(2.*C1*C2)))/                   np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))) +                 (ga*(El*g1 + I0))/                 np.sqrt((-(C2*g1) - C1*g2 - C1*ga - C2*ga)**2 - 4*C1*C2*(g1*g2 + g1*ga + g2*ga))))/            (2.*C1*C2*((g1*g2)/(C1*C2) + (g1*ga)/(C1*C2) + (g2*ga)/(C1*C2))))/         np.exp(((C2*g1 + C1*g2 + C1*ga + C2*ga -                 np.sqrt(C2**2*g1**2 - 2*C1*C2*g1*g2 + C1**2*g2**2 - 2*C1*C2*g1*ga + 2*C2**2*g1*ga +                   2*C1**2*g2*ga - 2*C1*C2*g2*ga + C1**2*ga**2 + 2*C1*C2*ga**2 + C2**2*ga**2))*t)/ (2.*C1*C2))))/ga


def IC_membrane_test(filename, return_fig=False, return_with_infos=False,\
                    command_channel=1,                     
                    delay_before_Rm_eval=100e-3):
    """
    t_pre and t_post set the windows of sampling of the stationary
    currents :
    Ibase = I[t2-t_pre,t2], Iend=[t3-t_post,t3]
    they are also usefull for the visualization
    we visualize : [t2-t_pre, t3-t_post]
    """
    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    t, data = ep.get_signals_episode(filename)

    try:
        params = ep.get_metadata(filename) # metadata
    except IOError:
        print '---> metadata not found !'
    dt = t[1]-t[0]

    if return_fig:
        fig = plt.figure(figsize=FIGSIZE)
        plt.subplots_adjust(bottom=.13, right=.97, left=.135)
        sp1 = plt.subplot2grid((3,1), (0,0), rowspan=2)
        sp2 = plt.subplot2grid((3,1), (2,0))
        plt.suptitle('Current Clamp Membrane Test : '+time)
      
    mean_v_response = np.zeros(t.size)
    mean_i = np.zeros(t.size)

    
    for i in range(len(data)):
        if return_fig:
            sp1.plot(t, data[i][0], color='b', lw=0.5, alpha=.2)
        mean_v_response += data[i][0]/(len(data))
        mean_i += data[i][command_channel]/(len(data))

    DI = np.abs(np.diff(mean_i[5:])).max() # pA, pulse

    # we find where the pulse start !
    i1 = np.where(np.abs(np.diff(mean_i[5:]))>.6*DI)[0]
    step_times = np.where(np.abs(np.diff(mean_i[5:]))>.6*DI)[0]
    if len(step_times)==2:
        [i1, i2] = step_times
    else:
        if len(step_times)>2:
            [i1, i2] = step_times[0], step_times[1]
        else:
            i1, i2= 4000, 8000
        print '================================================='
        print '--------> problem with the pulse recognition '
        print '================================================='

    # then we discard the loading time of the membrane
    i1_bis = i1+int(delay_before_Rm_eval/dt)

    i_base = mean_i[5:i1-1].mean()
    i_end = mean_i[i1+2:i2-2].mean() # average over last 10 points and traces !
    v_base = mean_v_response[5:i1-1].mean()
    v_end = mean_v_response[i1_bis+1:i2-1].mean()
    
    t_fit = t[i1:i2]
    v_fit = mean_v_response[i1:i2]
    t_fit = t_fit-t_fit[0] # to start from zero !
    

    success_flag_1comp=False
    print "Single compartment fit :"
    try:
        [Rm, El, Cm], v_fit_1comp = make_fit_for_single_comp(\
            t[:i2], mean_v_response[:i2],i_end-i_base, t[i1])
        success_flag_1comp=True
        print "--------> Ok !"
    except RuntimeError:
        print "Fit for the Membrane Test not achieved !"

    success_flag_2comp=False
    print "Double compartment fit :"
    try:
        [RmS, CmS, Ra, RmD, CmD], v_fit_2comp = make_fit_for_double_comp(\
            t[:i2], mean_v_response[:i2],i_end-i_base, t[i1])
        success_flag_2comp=True
        print "--------> Ok !"
    except RuntimeError:
        print "Fit for the Membrane Test not achieved !"
        
    if return_fig:
        sp1.plot(t, mean_v_response, color='b', lw=1)
        sp2.set_xlabel('time (ms)')
        sp1.set_ylabel('$V_m$ (mV)')
        sp2.plot(t, mean_i, 'k', lw=1)
        sp2.set_ylabel('current (pA)')
        if success_flag_1comp:
            sp1.plot(t[:i2], v_fit_1comp, 'k--', lw=4, label='Single comp.')
            fig.text(.2,.25,'Single comp. :  $R_m$='+str(Rm)+'M$\Omega$, '+\
                 '  Cm='+str(Cm)+'pF'+'  El='+str(El)+'mV',
                 fontsize=13,backgroundcolor='lightgray')
        if success_flag_2comp:
            sp1.plot(t[:i2], v_fit_2comp, 'r-', lw=1, label='Double comp.')
        #     fig.text(.05,.13,'Double comp. :  $R_m^1$='+\
        #              str(round(RmS,1))+'M$\Omega$, $R_m^2$='+\
        #              str(round(RmD,1))+'M$\Omega$, $R_i$='+\
        #              str(round(Ra,1))+'M$\Omega$, $C_m^1$='+\
        #              str(round(CmS,1))+'pF, $C_m^2$='+\
        #              str(round(CmD,1))+'pF',
        #              fontsize=11,backgroundcolor='lightgray')

        sp1.legend(prop={'size':'x-small'}, loc='best')
        sp1.locator_params(nbins=4)
        sp2.locator_params(nbins=4)
        sp1.set_xlim([t.min(),t.max()])
        sp1.set_ylim([mean_v_response.mean()-2.*mean_v_response.std(),\
                      mean_v_response.mean()+2.*mean_v_response.std()])
        sp1.set_xticks([])
        sp2.set_xlim([t.min(),t.max()])
        if return_with_infos:
            return [fig, round(1e3/popt_1comp[0],1), popt_1comp[1]]
        else:
            return [fig]
    else:
        return t, mean_v_response

    
##### IV curve plot    

def IVcurve_plot(filename, units='ms'):
    t, data = ep.get_signals_episode(filename)
    params = ep.get_metadata(filename) # metadata
    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    fig = plt.figure(figsize=FIGSIZE)
    plt.suptitle(' IV-curve : '+time+', n='+str(len(data)))
    p1 = plt.subplot(111)
    sp1 = plt.axes([0.7,0.7,0.19,0.19]) # inset for full trace
    mymap = matplotlib.colors.LinearSegmentedColormap.from_list(\
                'mycolors',['black','red'])
    for i in range(len(data)):
        r = float(i-1)/float(len(data))
        p1.plot(t, data[i][0], color=mymap(r,1))
        sp1.plot(t, data[i][1], color=mymap(r,1))
    sp1.set_xticks([]);sp1.set_yticks([-90,-60,-30,0])
    sp1.set_ylabel('mV')
    p1.locator_params(nbins=4)
    p1.set_xlim([37,170])
    p1.set_xlabel('ms')
    p1.set_ylabel('pA')
    return [fig]    


###### --- single pulse experiment (usually to see spike adaptation)

def single_pulse_analysis(filename, units='ms', clamp='IC', discret_spikes=10,\
                crossing=-30, return_fig=False):
    t, data = ep.get_signals_episode(filename)
    params = ep.get_metadata(filename) # metadata
    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    spikes = np.empty(0)
    if units=='s':
        t = 1e-3*t
    fig = plt.figure(figsize=FIGSIZE)
    plt.locator_params(nbins=4)
    plt.suptitle(' PULSE : '+time+', n='+str(len(data)))
    sp1 = plt.subplot2grid((4,1), (0,0), rowspan=2)
    mymap = matplotlib.colors.LinearSegmentedColormap.from_list(\
                'mycolors',['black','red'])
    for i in range(len(data)):
        if return_fig:
            r = float(i-1)/float(len(data))
            sp1.plot(t, data[i][0], color=mymap(r,1))
        ss = find_crossing_time(t, data[i][0], crossing=crossing)
        spikes = np.concatenate([spikes,ss])
    BINS = np.arange(t.min(),t.max(),discret_spikes)
    hist = np.histogram(spikes, bins=BINS)
    if return_fig:
        sp1.set_xticks([])
        sp1.set_ylabel('mV')
        sp2 = plt.subplot2grid((4,1), (2,0))
        sp2.plot(t, data[1][1],'b', lw=2)
        sp2.set_xticks([])
        sp2.set_ylabel('pA')
        sp3 = plt.subplot2grid((4,1), (3,0))
        sp3.plot(BINS[1:], hist[0], 'k', lw=3)
        sp3.set_ylabel('spk #')
        sp3.set_xlabel('time ('+units+')')
        return [fig]
    else:
        return [BINS[1:], hist[0]]
    
###### --- Multiple Sinusoids Analysis
def impedance_analysis(filename, sliding_window=3, return_fig=True,\
                       ylabel="Impedance (M$\Omega'$)", position=[.65,.65],
                       clamp_int=1):
    """
    clamp_int = 1 or 2, is the index for the amplifier mode IC/VC
    we plot the impedance and try to calculate a slope in different
    frequency range, based on a sliding window
    ----
    sliding_window=2 is the number of points taken around each point to
    make the linear fit for the slope
    --
    position =[x0,y0] is the position of the inset plot 
    """
    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    data = np.loadtxt(filename, unpack=True)
    ss = np.argsort(data[0])
    freq, Imped = data[0][ss], data[1][ss]
    if clamp_int==2:# then we need to scitch from pS to MOhm
        Imped = 1e6/Imped
    # now trying to determine a slope !
    x,y = [], []
    for i in range(sliding_window,len(freq)-sliding_window):
        pol = np.polyfit(np.log(freq[i-sliding_window:i+sliding_window]),
                         np.log(Imped[i-sliding_window:i+sliding_window]),1)
        x.append(freq[i])
        y.append(pol[0])
    #then we have the phase
    phase = data[2][ss]%(2.*np.pi)
    # then we put it between -pi and pi
    phase = [p if p<3*np.pi/2. else 2*np.pi-p for p in phase]
    if return_fig:
        fig = plt.figure(figsize=FIGSIZE)
        if clamp_int==1: plt.suptitle('input imped. I-Clamp : '+time)
        else: plt.suptitle('input imped. V-Clamp : '+time)
        p1 = plt.subplot(111)
        plt.loglog(freq, Imped, 'kD-')
        plt.xlabel('frequency (Hz)')
        plt.ylabel(ylabel)
        p2 = p1.twinx()
        p2.semilogx(freq, phase, 'bD-',\
                    ms=4, lw=.5, alpha=.4)
        p2.set_ylabel('phase shift (Rd)')
        p2.set_yticks([-np.pi/2.,0,np.pi/2.,np.pi,3.*np.pi/2.,])
        p2.set_yticklabels(["-$\pi$/2","0","$\pi$/2","$\pi$","3$\pi$/2"])
    
        sp1 = plt.axes((position[0],position[1],0.2,0.2))
        sp1.semilogx(x, y)
        sp1.set_ylabel('slope', size='small')
        sp1.set_yticks([0,-.5,-.75,-1])
        sp1.set_yticklabels(["0","-0.5","","-1"])
        plt.grid()
        plt.tight_layout()
        return [fig]
    else:
        return freq, Imped, phase, np.array(x), np.array(y)
      

###### --- F-I curve experiments !!! (to see cell classes)

def FI_curve_analysis(filename, crossing=-30, return_fig=False):
    """
    we plot 4 levels, the first one, the one around the third,
    one around 2 third and last one
    """
    t, data = ep.get_signals_episode(filename)
    params = ep.get_metadata(filename) # metadata
    exp, time = ep.get_experiment_name_and_time(filename) # just for time

    II, FF = [], []
    for i in range(len(data)): # loop over episodes
        Iinj = data[i][2]
        i0 = np.min(np.where(Iinj[1:]>Iinj[:-1])) # first point where jump
        i1 = np.where(Iinj[1:]<Iinj[:-1])[0] # first point where decay
        II.append(Iinj[i0+1]-Iinj[i0])
        ss = find_crossing_time(t, data[i][0], crossing=crossing)
        FF.append(ss.size/(t[i1]-t[i0])*1e3) # firing rate
        
    if return_fig:
        fig = plt.figure(figsize=FIGSIZE)
        plt.suptitle(exp+' : '+time)
        plt.scatter(II, FF, marker='D', color='k')
        plt.xlabel('current step amp. (pA)')
        plt.ylabel('firing rate (Hz)')
        plt.tight_layout()
        return fig
    else:
        return II, FF
        
def FI_curve_traces_plot(filename, n1=None, n2=None):
    """
    we plot 4 levels, the first one, the one around the third,
    one around 2 third and last one, this is by default
    else you can choose the two middle index explicitely by
    setting n1 = 8 and n2= 24 (whatever)
    """
    t, data = ep.get_signals_episode(filename)
    params = ep.get_metadata(filename) # metadata
    exp, time = ep.get_experiment_name_and_time(filename) # just for time

    # tdur = float(params['pulse_duration'])
    tstart, tdur = 400., 800. #float(params['t1']), float(params['t2'])

    fig = plt.figure(figsize=FIGSIZE)
    plt.suptitle(' F-I curve traces : '+time+', sample of n='+str(len(data)))
    mymap = matplotlib.colors.LinearSegmentedColormap.from_list(\
                'mycolors',['black','red'])

    if n1 is None:
        n1 = int(len(data)/3.)
    if n2 is None:
        n2 = int(2.*len(data)/3.)

    ARRAY_OF_CHOOSEN_INDEX = [0, n1, n2, len(data)-1]
    for I in range(len(ARRAY_OF_CHOOSEN_INDEX)):
        r = float(ARRAY_OF_CHOOSEN_INDEX[I]-1)/float(len(data)-.99)
        vp1 = plt.subplot2grid((6,2), (3*int(I/2.),I%2), rowspan=2)
        ip1 = plt.subplot2grid((6,2), (3*int(I/2.)+2,I%2))
        
        vp1.plot(t, data[ARRAY_OF_CHOOSEN_INDEX[I]][0], color=mymap(r,1))
        ip1.plot(t, data[ARRAY_OF_CHOOSEN_INDEX[I]][1], 'b') # current plot
        vp1.set_xlim([tstart-50,tstart+tdur+50])
        ip1.set_xlim([tstart-50,tstart+tdur+50])
        ip1.set_ylim([data[ARRAY_OF_CHOOSEN_INDEX[I]][1].min(),\
                      1.2*data[ARRAY_OF_CHOOSEN_INDEX[I]][1].max()])
        ip1.locator_params(nbins=4)
        vp1.locator_params(nbins=3)
        if I==0 or I==2: vp1.set_ylabel('mV');ip1.set_ylabel('pA')
        if I==3 or I==2: ip1.set_xlabel('ms')


    fig2 = FI_curve_analysis(filename, crossing=-30, return_fig=True)
    return [fig, fig2]

### ### P over N protocol, for activation curves

def P_over_N_analysis(filename, return_fig=True, minus_sign=False):
    """
    Implementation
    """
    t, data = ep.get_signals_episode(filename)
    dt = t[1]-t[0]
    params = ep.get_metadata(filename) # metadata
    exp, time = ep.get_experiment_name_and_time(filename) # just for time

    nblank, nsignal = int(params['n_repeat_blank']),\
      int(params['n_repeat_signal'])
    V_scan = np.linspace(float(params['v_start']), float(params['v_end']),\
                         int(params['step_number']))

    ss=1
    if not minus_sign: # in the blank protocols
        ss = -1

    i00 = int(float(params['del_prepulse'])/dt)+1
    i0 = int(float(params['dur_prepulse'])/dt)+1+i00
    i1 = int(float(params['dur_pulse'])/dt)+i0+i00-2
    
    II, FF = [], []
    for i in range(len(data)): # loop over episodes
        step = int(i/(nblank+nsignal))
        substep = i%(nblank+nsignal)
        if substep<nsignal: # means signal
            if substep==0:
                II.append(data[i][0]/nsignal)
            else:
                II[-1] += data[i][0]/nsignal
        else: # means blank
            II[-1] += ss*data[i][0]

    V_scan = np.round(V_scan[:len(II)],1) # maybe we've not finished the protocol

    # then we do the i v curve analysis
    max_i = 0.*V_scan
    
    for ii in range(len(II)):
        i_max = np.argmax(np.abs(II[ii][i0:i1]))
        max_i[ii] = II[ii][i0:i1][i_max]

    # then linear regression on the last points
    
    if return_fig:
        [fig0] = naive_episode_plot(filename)
        fig1, fig2, fig3 = PoverN_plot_and_final_analysis(V_scan, II, max_i, exp=exp+' '+time)
        return [fig0, fig1, fig2, fig3]
    else:
        return V_scan, II, max_i

    
def PoverN_plot_and_final_analysis(V_scan, II, max_i, exp='what exp?',\
                      number_of_points_for_finding_Na=5):

    fig1 = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=0.15, left=.14, right=0.85, top=0.92)
    ax1 = plt.subplot2grid((6,10),(0,0), colspan=9, rowspan=6)
    ax2 = plt.subplot2grid((6,10),(1,9), rowspan=4)

    cmap = matplotlib.colors.LinearSegmentedColormap.from_list(\
        'mycolors',['red','blue'])
    bounds= np.linspace(V_scan.min()-1, V_scan.max()+1, len(V_scan)+1)
    norm = matplotlib.colors.BoundaryNorm(bounds, cmap.N)
    cb1 = matplotlib.colorbar.ColorbarBase(ax2, cmap=cmap, norm=norm,
                                orientation='vertical',
                                label = 'V pulse (mV)',
                                ticks=V_scan)
    plt.suptitle(exp)
    for ii in range(len(II)):
        cc = (V_scan[ii]-V_scan.min())/(V_scan.max()-V_scan.min())
        ax1.plot(II[ii], color=cmap(cc,1), lw=.5)
    ax1.set_ylabel('current (pA)')
    ax1.set_xlabel('time (ms)')

    fig2 = plt.figure()
    plt.suptitle(exp)
    plt.subplots_adjust(bottom=0.15, left=.14, right=0.95, top=0.92)
    plt.plot(V_scan, max_i, 'kD-', ms=5)
    plt.ylabel('current (pA)')
    plt.xlabel('V (mV)')

    # linear fit of the I-V curve to find the reversal pot
    r_Vscan, r_max_i = V_scan[-number_of_points_for_finding_Na:],\
      max_i[-number_of_points_for_finding_Na:]
    p0 = np.polyfit(r_Vscan, r_max_i, 1)
    plt.plot(r_Vscan, np.polyval(p0, r_Vscan), 'k--', lw=3, alpha=.5)

    Ena = -p0[1]/p0[0]
    print 'Estimated reversal potential :', Ena
    max_G = max_i/(V_scan-Ena)
    print 'Estimated max conductance :', -p0[1]/p0[0]
    
    fig3 = plt.figure()
    plt.suptitle(exp)
    plt.subplots_adjust(bottom=0.15, left=.14, right=0.95, top=0.92)
    plt.plot(V_scan, max_G, 'kD-', ms=5)
    plt.ylabel('max conductance (nS)')
    plt.xlabel('V (mV)')
    
    return [fig1, fig2, fig3]


def OU_continuous_plot(filename, return_fig=False):
    """
    the limit_percentage in [0,.5] is done to limit the range of plot
    """
    params = ep.get_metadata(filename) # metadata
    [fig] = naive_continuous_plot(filename, return_fig=True)
    fig.text(.7,.7,'$\mu=$'+str(float(params['ou_mean']))+'pA \n'+\
        '$\sigma=$'+str(float(params['ou_var']))+'pA \n'+\
        '$\\tau$='+str(float(params['ou_tau']))+'ms',\
        backgroundcolor='lightgray')
    return [fig]

###### everytime you want to plot the trace of a continuous mode exp
# where you want to count the spikes, use this

def naive_continuous_plot(filename, return_fig=False,t_max=10):
    """
    t_max limits the range of the plot ! (not to have too many points !)
    """
    t, data = ep.get_signals_continuous(filename)

    params = ep.get_metadata(filename) # metadata
    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    maybe_max, maybe_min = data[2].min(), data[2].max()

    fig = plt.figure(figsize=FIGSIZE)
    plt.suptitle(exp+' : '+time)
    sp1 = plt.subplot2grid((3,1), (0,0), rowspan=2)
    sp2 = plt.subplot2grid((3,1), (2,0))
    
    # no more than 10 seconds of plot
    dt = t[1]-t[0]
    i_max = min([int(t_max/dt),len(t)])
    new_t = t[:i_max]
    sp1.plot(new_t,data[0][:i_max], 'k')
    sp2.plot(new_t,data[1][:i_max], 'r')
    sp1.set_xlim([new_t.min(), new_t.max()])
    sp2.set_xlim([new_t.min(), new_t.max()])
    sp1.set_xticks([])

    if int(params['clamp_int'])==1:
        sp1.set_ylabel('mV')
        sp2.set_ylabel('pA')
    if int(params['clamp_int'])==2:
        sp2.set_ylabel('mV')
        sp1.set_ylabel('pA')
    fig.text(.7,.2,'$trace :$='+str(round(100.*i_max/len(t)))+'%',\
        backgroundcolor='lightgray')
    sp2.set_xlabel('s')
    return [fig]

def naive_episode_plot(filename):
    t, data = ep.get_signals_episode(filename)
    params = ep.get_metadata(filename) # metadata
    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    mymap = matplotlib.colors.LinearSegmentedColormap.from_list(\
                'mycolors',['black','red'])

    fig = plt.figure(figsize=FIGSIZE)
    plt.subplots_adjust(bottom=.15, right=.97, left=.16)
    plt.suptitle(exp+' : '+time)
    sp1 = plt.subplot2grid((3,1), (0,0), rowspan=2)
    plt.locator_params(nbins=5)
    sp2 = plt.subplot2grid((3,1), (2,0))
    plt.locator_params(nbins=2)
    for i in range(len(data)):
        r = float(i-1)/float(len(data))
        sp1.plot(t, data[i][0], color=mymap(r,1))
        sp2.plot(t, data[i][1], color=mymap(r,1))
    sp1.locator_params(nbins=4)
    sp2.locator_params(nbins=3)
    sp1.set_xticks([])
    sp2.set_xlabel('time (ms)')
    if int(params['clamp_int'])==1:
        sp1.set_ylabel('mV')
        sp2.set_ylabel('pA')
    if int(params['clamp_int'])==2:
        sp2.set_ylabel('mV')
        sp1.set_ylabel('pA')
    return [fig]

def naive_continuous_plot_ef(filename):
    t, data = ep.get_signals_continuous(filename)
    params = ep.get_metadata(filename) # metadata
    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    maybe_max, maybe_min = data[3].min(), data[3].max()

    fig = plt.figure(figsize=FIGSIZE)
    plt.suptitle(exp+' : '+time)
    sp1 = plt.subplot2grid((3,1), (0,0), rowspan=2)
    sp1.plot(t, data[1], 'k')
    sp1.set_xticks([])
    sp2 = plt.subplot2grid((3,1), (2,0))
    sp2.plot(t,data[3],'r', lw=1)
    sp2.set_yticks([maybe_min, data[3].mean(), maybe_max])
    sp2.set_ylabel('$\mu$A')
    sp1.set_xlim([t.min(),t.max()]);sp2.set_xlim([t.min(),t.max()])
    if int(params['clamp_int'])==1:
        sp1.set_ylabel('mV')
    if int(params['clamp_int'])==2:
        sp1.set_ylabel('pA')
    return [fig]


def naive_episode_plot_ef(filename):
    """
    only for current clamp
    """
    t, data = ep.get_signals_episode(filename)
    params = ep.get_metadata(filename) # metadata
    exp, time = ep.get_experiment_name_and_time(filename) # just for time
    mymap = matplotlib.colors.LinearSegmentedColormap.from_list(\
                'mycolors',['black','red'])

    fig = plt.figure(figsize=FIGSIZE)
    plt.suptitle(exp+' : '+time)
    sp1 = plt.subplot2grid((3,1), (0,0), rowspan=2)
    plt.locator_params(nbins=5)
    sp2 = plt.subplot2grid((3,1), (2,0))
    plt.locator_params(nbins=2)
    for i in range(len(data)):
        r = float(i-1)/float(len(data))
        sp1.plot(t, data[i][0], color=mymap(r,1))
        sp2.plot(t, data[i][2], color=mymap(r,1))
    sp1.set_xticks([])
    sp1.set_xlim([t.min(),t.max()]);sp2.set_xlim([t.min(),t.max()])
    sp2.set_ylabel('$\mu$A')
    if int(params['clamp_int'])==1:
        sp1.set_ylabel('mV')
    if int(params['clamp_int'])==2:
        sp1.set_ylabel('pA')
    return [fig]



###### general function that handles this
    
def analysis(filename, return_fig=True, crossing=-5,\
             command_channel=1):
    """
    take the filename of this and returns the classic analysis
    associated to this protocol !
    """
    exp, time = ep.get_experiment_name_and_time(filename)
    print filename
    if exp=='SINGLE_PULSE':
        fig = single_pulse_analysis(filename, return_fig=True, crossing=crossing)
    elif exp=='PULSE_TRAIN':
        fig = naive_episode_plot(filename)#pulse_train_analysis(filename, return_fig=True, crossing=crossing)
    elif exp=='VC_MEMBRANETEST':
        # fig = VC_membrane_test(filename, return_fig=True)
        fig1, _, _ = electrophy.VC_membrane_test.plot(filename)
        fig = [fig1]
    elif exp=='IC_MEMBRANETEST':
        # fig = IC_membrane_test(filename, return_fig=True,\
        #                        command_channel=command_channel)
        fig1, _, _ = electrophy.IC_membrane_test.plot(filename)
        fig = [fig1]
    elif exp=='OU_NOISE':
    #     fig = naive_continuous_plot(filename, crossing=crossing, return_fig=True)
        fig1, fig2, _ = electrophy.spike_phase_space.plot(filename)
        fig = [fig1, fig2]
    elif exp=='IV_CURVE':
        fig = IVcurve_plot(filename)
    elif exp=='FI-CURVE':
        fig = FI_curve_traces_plot(filename)
    elif exp=='VCLAMP_POVERN':
        fig = P_over_N_analysis(filename)
        fig = [fig[0]]
    elif exp=='OU_NOISE':
        # fig = OU_continuous_plot(filename)
        fig = electrophy.spike_phase_space.plot(filename)
    elif exp=='MULTIPLE_SIN':
        fig1, fig2, fig3, _, _, _ = electrophy.impedance.plot(filename)
        fig = [fig1, fig2, fig3]
        # analyzed = filename.replace('1.DAT','_analysed.txt')
        # params = ep.get_metadata(filename) # metadata
        # clamp_int = int(params['clamp_int']) # 1 is IC, 2 is VC
        # print clamp_int
        # if os.path.isfile(analyzed):
        #     [fig2] = impedance_analysis(analyzed, return_fig=True, clamp_int=clamp_int)
        #     fig2.savefig('figs/'+filename.replace('1.DAT','_2.png'))
        #     print 'FULL ANALYSIS done and saved !'
        # else:
        #     print 'need to pre-analyze in Elphy :'+filename+' and then use the function impedance_analysis()'
        # fig = naive_continuous_plot(filename, t_max=70)
    elif exp=='EF_MULTIPLE_SIN':
        analyzed = filename.replace('1.DAT','_analysed.txt')
        print 'need to pre-analyze in Elphy :'+filename+' and then use the function impedance_analysis()'
        t, data = ep.get_signals_continuous(filename)
        fig = plt.figure(figsize=FIGSIZE)
        plt.suptitle(exp)
        plt.subplot2grid((3,1),(0,0),rowspan=2);plt.plot(t[:max(len(data),10000)],data[0][:max(len(data),10000)],'k-')
        plt.subplot2grid((3,1),(2,0));plt.plot(t[:max(len(data),10000)],data[2][:max(len(data),10000)],'r-')
    elif exp=='SINGLE_PULSE_EFIELD':
        fig = naive_episode_plot_ef(filename)
    elif exp=='VARYING_PULSES_EFIELD':
        fig = naive_episode_plot_ef(filename)
    else:
        params = ep.get_metadata(filename) # metadata
        if (params['cont_choice']==True):
            fig = naive_continuous_plot(filename)
        else:
            fig = naive_episode_plot(filename)
        print "Filename not recognized (so naive plot) :"+filename
    print " |-------> ANALYZED"
    return fig

    
