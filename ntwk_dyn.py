#!/usr/bin/python
# Filename: ntwk_dyn.py

from neuron import h as nrn
import numpy as np
import matplotlib.pylab as plt
import numpy.fft as fft

def load_nrnVecFile(stringPath):
    file = nrn.File()
    nrnVec = nrn.Vector()
    file.ropen(stringPath)
    nrnVec.vread(file)
    file.close
    return np.array(nrnVec)

def load_raster(string='output'):
    """
    argument : folder where all the quantoties are !
    this functions returns a list of np.array()
    they correspond to :
    
    spkIDs_exc = np.array(load_nrnVecFile(string+"/idvecEXC"),dtype=int)
    spkTimes_exc = load_nrnVecFile(string+"/timevecEXC")
    spkIDs_inh = np.array(load_nrnVecFile(string+"/idvecINH"),dtype=int)
    spkTimes_inh = load_nrnVecFile(string+"/timevecINH")

    """
    Raster = []
    Raster.append(np.array(load_nrnVecFile(string+"/idvecEXC"),dtype=int))
    Raster.append(np.array(load_nrnVecFile(string+"/timevecEXC")))
    Raster.append(np.array(load_nrnVecFile(string+"/idvecINH"),dtype=int))
    Raster.append(np.array(load_nrnVecFile(string+"/timevecINH")))
    return Raster


def load_VmTraces(string='output'):
    Vm = []
    for i in range(10):
        Vm.append(load_nrnVecFile(string+"/Vm"+str(i)))
    return Vm

def vm_plot(DICT,string='output',sv=False):
    Vm = load_VmTraces(string)
    t = np.arange(0,DICT['Tstop']+DICT['DTntwk'],DICT['DTntwk'])
    fig = plt.figure(figsize=(15,8))
    for i in range(len(Vm)-1):
        vm = fig.add_subplot(911+i)
        vm.plot(t,Vm[i])
        vm.set_yticks([-70,-60,-50])
    vm.set_xlabel('time in ms')
    if sv:
        return fig


    
def load_FieldTraces(string='output'):
    Field = []
    for i in range(10):
        Field.append(load_nrnVecFile(string+"/DV"+str(i)))
    return Field

def ef_plot(DICT,string='output',sv=False):
    Field = load_FieldTraces(string)
    t = np.arange(0,DICT['Tstop']+DICT['DTntwk'],DICT['DTntwk'])
    t = t[:Field[0].size] # in case of size issues
    print t.size
    print Field[0].size
    fig = plt.figure(figsize=(15,8))
    for i in range(len(Field)):
        vm = fig.add_subplot(911+i)
        vm.plot(t,Field[i])
        vm.set_yticks([-70,-60,-50])
    vm.set_xlabel('time in ms')
    if sv:
        return fig

def meanEF_plot(DICT,string='output',sv=False):
    Field = load_FieldTraces(string)
    t = np.arange(0,DICT['Tstop']+DICT['DTntwk'],DICT['DTntwk'])
    t = t[:Field[0].size] # in case of size issues
    meanef = np.zeros(t.size)
    stdef = np.zeros(t.size)
    for i in range(meanef.size):
        for j in range(len(Field)):
            meanef[i]+=Field[j][i]/len(Field)
        for j in range(len(Field)):
            stdef[i]+=np.sqrt((meanef[i]-Field[j][i])**2)/len(Field)
    fig = plt.figure(figsize=(15,5))
    vm = fig.add_subplot(111)
    vm.plot(t,meanef,'k')
    emin = np.array(meanef-stdef)
    emax = np.array(meanef+stdef)
    vm.fill_between(t,emax,emin,color='k',alpha=.2)
    vm.set_xlabel('time in ms')
    vm.set_ylim([1.4*emin.mean(),0.6*emax.mean()])
    if sv:
        return fig

def meanEF(DICT,string='output',sv=False):
    Field = load_FieldTraces(string)
    t = np.arange(0,DICT['Tstop']+DICT['DTntwk'],DICT['DTntwk'])
    t = t[:Field[0].size] # in case of size issues
    meanef = np.zeros(t.size)
    for i in range(meanef.size):
        for j in range(len(Field)):
            meanef[i]+=Field[j][i]/len(Field)
    return meanef
    


# --- === --- Reading parameters !

def read_params(string='output'):
    """
    take the path as arguments.
    returns the dictionnary for the parameters 
    """
    DICT = dict()
    ntwkParams = load_nrnVecFile(string+"/Param")
    DICT['Ncell'] = int(ntwkParams[0])
    DICT['connectivity'] = ntwkParams[1]
    DICT['Qe'] = ntwkParams[2]
    DICT['Qi'] = ntwkParams[3]
    DICT['TauE'] = ntwkParams[4]
    DICT['TauI']= ntwkParams[5]
    DICT['EE']= ntwkParams[6]
    DICT['Ei']= ntwkParams[7]
    DICT['g']= ntwkParams[8]
    DICT['NEURON']= ntwkParams[9]
    DICT['EP_yes/no'] = ntwkParams[10]
    DICT['Tstop'] = ntwkParams[11]
    DICT['DTntwk'] = ntwkParams[12]
    DICT['EPstrength'] = ntwkParams[13]
    DICT['iaf_refrac'] = ntwkParams[14]
    return DICT

def findSpikes(Ncell,List):
    """
    args : Ncell, and LIST[3] (idExc,tExc,idInh,tInh)
    returns the Tspike list , vectors of spike times for each neuron
    """
    idExc = List[0]
    tExc = List[1]
    idInh = List[2]
    tInh = List[3]
    if (tExc.size==idExc.size):
        if (tInh.size==idInh.size):
            print "\n"+"vector sizes are ok !!"+"\n"
    else:
        " problem with the vector sizes"
    Tspike = [np.array([]) for j in range(Ncell)] 
    for j in np.unique(idExc):
        Tspike[j-1]=(tExc[idExc==j])
    for j in np.unique(idInh):
        Tspike[j-1]=(tInh[idInh==j])
    return Tspike

def RasterPlot(List,DICT,sv=False):
    """
    take the List of Raster output
    returns the grah if sv=True
    """
    spkIDs_exc = List[0]
    spkTimes_exc = List[1]
    spkIDs_inh = List[2]
    spkTimes_inh = List[3]
    fig = plt.figure(figsize=(15,8))
    rs = fig.add_subplot(111)
    rs.scatter(spkTimes_exc,spkIDs_exc, s=.5, c='b',lw=0) 
    rs.scatter(spkTimes_inh,spkIDs_inh, s=.5, c='r',lw=0) 
    rs.set_xlabel('time in ms',fontsize=15)
    rs.set_ylabel('neuron identity',fontsize=15)
    rs.set_xlim(0,DICT['Tstop'])
    rs.set_ylim(0,DICT['Ncell'])
    if sv:
        return fig



def RasterPlot_ordered(List,DICT,sv=False):
    """
    take the List of Raster output
    returns the grah if sv=True
    """
    spkIDs_exc = List[0]
    spkTimes_exc = List[1]
    spkIDs_inh = List[2]
    spkTimes_inh = List[3]
    ID = 1
    spkIDs_exc2 = np.copy(spkIDs_exc)
    for i in np.unique(spkIDs_exc): 
        spkIDs_exc2[spkIDs_exc==i]=ID
        #for j in spkIDs_exc2: spkIDs_exc2[spkIDs_exc==i]=ID
        ID+=1
    spkIDs_inh2 = np.copy(spkIDs_inh)
    for i in np.unique(spkIDs_inh): 
        spkIDs_inh2[spkIDs_inh==i]=ID
        #for j in spkIDs_inh2: spkIDs_inh2[spkIDs_inh==i]=ID
        ID+=1
    print "the last id is : ",ID,"\n \n"
    fig = plt.figure(figsize=(15,8))
    rs = fig.add_subplot(111)
    rs.scatter(spkTimes_exc,spkIDs_exc2, s=.5, c='b',lw=0) 
    rs.scatter(spkTimes_inh,spkIDs_inh2, s=.5, c='r',lw=0) 
    rs.set_xlabel('time in ms',fontsize=15)
    rs.set_ylabel('neuron identity',fontsize=15)
    rs.set_xlim(0,DICT['Tstop'])
    rs.set_ylim(0,DICT['Ncell'])
    if sv:
        return fig

def computActivity(List,dict,BIN):
    """
    argument : List of Raster data
    ,BIN size (that we want) and dict() of the params
    
    returns a firing frequency per bin, for the excitation, inhibition
    and total activity,.  we count the number of spikes per bin, we
    divide it by the bin size and the number of cell in the population
    (and adjust *1000 -> Hz). For the total population acivity we take
    into account the weight of the number of cells. Act = (1-g)*Exc +
    g*Inh
    """
    spkIDs_exc = List[0]
    spkTimes_exc = List[1]
    spkIDs_inh = List[2]
    spkTimes_inh = List[3]
    t = np.arange(0,dict['Tstop'],BIN)
    smplAct_exc = np.histogram(spkTimes_exc/BIN,bins=t.size)[0]
    smplAct_exc = smplAct_exc*1000/((1-dict['g'])*dict['Ncell']*BIN)
    smplAct_inh = np.histogram(spkTimes_inh/BIN,bins=t.size)[0]
    smplAct_inh = smplAct_inh*1000/(dict['g']*dict['Ncell']*BIN)
    smplAct = (1-dict['g'])*smplAct_exc + dict['g']*smplAct_inh
    return t,smplAct,smplAct_exc,smplAct_inh

def PlotActivity(List,dict,BIN,sv=False):
    """
    argument : List of Raster data
    ,BIN size (that we want) and dict() of the params

    plots the activity in time
    returns the figure if sv=True
    """
    t,smplAct,smplAct_exc,smplAct_inh = computActivity(List,dict,BIN)
    fig = plt.figure(figsize=((15,6)))
    act = fig.add_subplot(111)
    act.plot(t,smplAct_exc,'b--',lw=1)
    act.plot(t,smplAct_inh,'r--',lw=1)
    act.plot(t,smplAct,'k',lw=2)
    act.set_title("Network activity sampled in bins of "+ \
                      str(BIN)+ " ms",fontsize=12)
    act.set_xlabel("time in ms",fontsize=12)
    act.set_ylabel("cummulated firing rate (within bins)",fontsize=12)
    if sv: 
        return fig


def PlotHistogram(List,dict,BIN,sv=False):
    t,smplAct,smplAct_exc,smplAct_inh = computActivity(List,dict,BIN)
    fig = plt.figure(figsize=((15,6)))
    act = fig.add_subplot(111)
    plt.hist(smplAct_exc, 50, normed=1, facecolor='blue', alpha=0.5)
    plt.hist(smplAct_inh, 50, normed=1, facecolor='red', alpha=0.5)
    plt.hist(smplAct, 50, normed=1, facecolor='black', alpha=0.75)
    act.set_title("Network activity Histogram  sampled in bins of "+ \
                      str(BIN)+ " ms",fontsize=12)
    act.set_xlabel("Activity in Hz",fontsize=12)
    act.set_ylabel("occurence",fontsize=12)
    if sv: 
        return fig

def autocorrel(signal):
    """
    take a signal, and returns its autocorrelated function 
    (normalized)
    """
    signal = (signal-signal.mean())/signal.std()
    cr = np.correlate(signal,signal,"full")
    return cr/cr.max()


def CorrelComput(List,dict,BIN):
    """
    we compute the autocorrellation function of the 3 signals, we use
    np.correlate, nevertheless, this function just do a stupid sum of
    the term. For autocorrelation plotting, it doesn;t take into
    account, the fact that we summ much more on the 0 point than on
    the boundaries, we need to correct for this !
    """
    t,smplAct,smplAct_exc,smplAct_inh =computActivity(List,dict,BIN)

    autoC = autocorrel(smplAct)
    autoC_exc = autocorrel(smplAct_exc)
    autoC_inh = autocorrel(smplAct_inh)

    t = np.arange(0,BIN*autoC.size,BIN)
    t = t - t.max()/2 
    return t,autoC,autoC_exc,autoC_inh


def PlotCorrelation(List,dict,BIN,sv=False):
    t,autoC,autoC_exc,autoC_inh = CorrelComput(List,dict,BIN)

    fig =plt.figure(figsize=((15,6))) 
    act = fig.add_subplot(111)

    act.plot(t,autoC,'k-',lw=2)
    act.plot(t,autoC_exc,'b--',lw=1)
    act.plot(t,autoC_inh,'r--',lw=1)
    act.set_title("Autocorrelation function of the Activity "+ \
                      str(BIN)+ " ms",fontsize=12) 
    act.set_xlabel("time delay in ms",fontsize=12) 
    act.set_ylabel("coefficient",fontsize=12) 
    if sv:
        return fig



def PlotSpectrum(List,dict,BIN,sv=False):
    """
    spctrm plot
    """
    t,smplAct,smplAct_exc,smplAct_inh =computActivity(List,dict,BIN)

    fx,tf,phase = fourierreal(smplAct,BIN*1e-3)
    fx,tf_exc,phase_exc = fourierreal(smplAct_exc,BIN*1e-3)
    fx,tf_inh,phase_inh = fourierreal(smplAct_inh,BIN*1e-3)
    

    fig =plt.figure(figsize=((15,6))) 
    act = fig.add_subplot(111)

    act.semilogy(fx,tf,'k-',lw=2)
    act.semilogy(fx,tf_exc,'b--',lw=1)
    act.semilogy(fx,tf_inh,'r--',lw=1)

    act.set_title("Power spectrum of the Activity in "+ \
                      str(BIN)+ " ms bins",fontsize=12) 
    act.set_xlabel("frequency in Hz",fontsize=12) 
    act.set_ylabel("power",fontsize=12) 
    if sv:
        return fig



def fourierreal(signal,dt):
    """
    arguments :
    signal -> 1D numpy array (has to be real)
    dt -> real
    
    returns :
    the frequency vector -> np.array()
    the psd vector -> np.array()
    the phase vector in degree !! -> np.array()
    """
    N = signal.size
    if N%2==0:
        Np=int(N/2+1)
    else:
        Np=int((N+1)/2)
    freq = fft.fftfreq(N,dt)[0:Np]
    freq[-1]=-freq[-1] # because else is -N/2 in case N is even !
    ft = fft.rfft(signal)
    psd = abs(ft)
    phase = np.angle(ft,deg=True)
    return freq,psd,phase
    
def set_stuff(param):
    """
    argument : the dict of parameters

    -> t,dt,tstop
    """
    dt = param['DTntwk']/1e3
    tstop = param['Tstop']/1e3
    t = np.arange(0,tstop+dt,dt)
    return t,dt,tstop

def ACTandEF_plot(List,DICT,BIN,string='output',sv=False):
    Field = load_FieldTraces(string)
    t = np.arange(0,DICT['Tstop']+DICT['DTntwk'],DICT['DTntwk'])
    t = t[:Field[0].size] # in case of size issues
    meanef = np.zeros(t.size)
    stdef = np.zeros(t.size)
    for i in range(meanef.size):
        for j in range(len(Field)):
            meanef[i]+=Field[j][i]/len(Field)
        for j in range(len(Field)):
            stdef[i]+=np.sqrt((meanef[i]-Field[j][i])**2)/len(Field)
            
    t = t[int(100/DICT['DTntwk']):] # rescaling so that we start at 100ms
    meanef = meanef[int(100/DICT['DTntwk']):]
    meanef = (meanef-meanef.mean())/meanef.std()
    
    fig = plt.figure(figsize=(15,8))
    vm = fig.add_subplot(211)
    vm.plot(t,meanef,'k')
    vm.set_xlabel('time in ms')
    vm.set_ylabel('mean electric field (normalized)')
    
    t,smplAct,smplAct_exc,smplAct_inh = computActivity(List,DICT,BIN)

    t = t[int(100/BIN):] # rescaling
    smplAct = smplAct[int(100/BIN):]
    smplAct = (smplAct-smplAct.mean())/smplAct.std()
    vm2 = fig.add_subplot(212)
    vm2.plot(t,smplAct,'k')
    vm2.set_xlabel('time in ms')
    vm2.set_ylabel('network activity (normalized)')
    if sv:
        return fig
