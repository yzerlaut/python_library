import numpy as np

## pearson correlation
from scipy.stats.stats import pearsonr

def pearson_correl(X, Y, ax=None):
    cc, pp = pearsonr(X, Y)
    x = np.linspace(X.min(), X.max())
    if ax is not None:
        ax.plot(x, np.polyval(np.polyfit(np.array(X, dtype='float'),\
                np.array(Y, dtype='float'), 1), x), 'k--', lw=.5)
        ax.annotate('c='+str(np.round(cc,1))+', p='+'%.1e' % pp,\
                     (0.15,1), xycoords='axes fraction')

    return cc, pp
