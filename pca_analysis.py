import numpy as np

def pca(A):
 """ performs principal components analysis 
     (PCA) on the n-by-p data matrix A
     Rows of A correspond to observations, columns to variables. 

 Returns :  
  coeff :
    is a p-by-p matrix, each column containing coefficients 
    for one principal component.
  score : 
    the principal component scores; that is, the representation 
    of A in the principal component space. Rows of SCORE 
    correspond to observations, columns to components.
  latent : 
    a vector containing the eigenvalues 
    of the covariance matrix of A.
 """
 # computing eigenvalues and eigenvectors of covariance matrix
 M = (A-np.mean(A.T,axis=1)).T # subtract the mean (along columns)
 [latent,coeff] = np.linalg.eig(np.cov(M))
 p = np.size(coeff,axis=1)
 idx = np.argsort(latent) # sorting the eigenvalues
 idx = idx[::-1]       # in ascending order
 # sorting eigenvectors according to the sorted eigenvalues
 coeff = coeff[:,idx]
 latent = latent[idx] # sorting eigenvalues
 score = np.dot(coeff.T,M) # projection of the data in the new space
 return coeff,score,latent



if __name__=='__main__':

    import matplotlib.pylab as plt
    from my_graph import set_plot
    
    t = np.linspace(0, 1, 5) # time axis, THIS IS THE VARIABLE !!
    n_sample = 30
    data = np.zeros((n_sample,len(t))) # 20 trials
    factor= 1. # will determine the strength of the comodulation !!
    for i in range(n_sample):
        data[i,:] = factor*(.5-np.random.randn())*np.sin(2.*np.pi*t)**2+np.random.randn(len(t))+10.*np.exp(-t)
    A = data

    # visualizing the data
    for i in range(n_sample):
        plt.plot(t, data[i,:])
    set_plot(plt.gca(), xlabel='features', ylabel='data value')
    plt.show()
    
    coeff, score, latent = pca(A)
    fig, ax = plt.subplots(4, figsize=(7,6))
    for i in range(4):
        ax[i].set_title('var. explained '+str(round(100.*latent[i]/np.sum(latent)))+'%')
        ax[i].bar(t,coeff[:,i], color='k', width=(t[1]-t[0])/4.)
        set_plot(ax[i], 'left', ylabel='p.c. coeff.', xticks=[])
        ax[i].legend(frameon=False, prop={'size':'x-small'})
    set_plot(ax[i], ['bottom','left'], xlabel='features', ylabel='p.c. coeff.')
    plt.show()
