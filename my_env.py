import matplotlib.pylab as plt
import matplotlib as mpl
import numpy as np
# import sys
# sys.path.append("/home/yann/files/my_libs/python/")

# possible fonts
# mpl.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
# mpl.rc('font',**{'family':'sans-serif','sans-serif':['Avant Garde']})
# mpl.rc('font',**{'family':'sans-serif','sans-serif':['Computer Modern Sans Serif']})
# mpl.rc('font',**{'family':'serif','serif':['Times']}) # --> NICE
# mpl.rc('font',**{'family':'serif','serif':['Palatino']})
# mpl.rc('font',**{'family':'serif','serif':['New Century Schoolbook']})
# mpl.rc('font',**{'family':'serif','serif':['Bookman']})
# mpl.rc('font',**{'family':'cursive','cursive':['Zapf Chancery']})

## TOGETHER WITH LATEX, use :
# mpl.rc('font',**{'family':'serif','serif':['Computer Modern Roman']})
# mpl.rc('font',**{'family':'serif','serif':['Times New Roman']})

# import matplotlib.font_manager as font_manager
# path = '/usr/share/fonts/truetype/ttf-lyx/cmr10.ttf'
# prop = font_manager.FontProperties(fname=path)
# mpl.rcParams['font.family'] = prop.get_name()
# mpl.rc('text', usetex=True)

# mpl.rc('font',**{'size':16})
# mpl.rc('axes',**{'unicode_minus': False})

