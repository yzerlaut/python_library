#!/usr/bin/python
# Filename: single_cell.py

#from neuron import h as nrn
import numpy as np
import matplotlib.pylab as plt

def trace_plot(out):
    fig = plt.figure(figsize=(15,15))
    # vm
    f1 = plt.subplot(311)
    f1.plot(out[0],out[1],'k',alpha=.6)
    mean_vm = out[1].mean()*np.ones(out[0].size)
    f1.plot(out[0],mean_vm,'k--',linewidth=2)
    f1.plot(out[0],-50*np.ones(out[0].size),'r-',linewidth=1)
    std_vm = out[1].std()*np.ones(out[0].size)
    f1.fill_between(out[0],mean_vm-std_vm,mean_vm+std_vm,color='k',alpha=.3)
    f1.set_xlim(0,out[0].max())
    f1.set_ylabel('vm (mV)')
    # ge
    f2 = plt.subplot(312)
    f2.plot(out[0],out[2],'b',alpha=.6)
    mean_ge = out[2].mean()*np.ones(out[0].size)
    f2.plot(out[0],mean_ge,'b--',linewidth=2)
    std_ge = out[2].std()*np.ones(out[0].size)
    f2.fill_between(out[0],mean_ge-std_ge,mean_ge+std_ge,color='b',alpha=.3)
    f2.set_xlim(0,out[0].max())
    f2.set_ylabel('excitation (uS)')
    # gi
    f3 = plt.subplot(313)
    f3.plot(out[0],out[3],'r',alpha=.6)
    mean_gi = out[3].mean()*np.ones(out[0].size)
    f3.plot(out[0],mean_gi,'r--',linewidth=2)
    std_gi = out[3].std()*np.ones(out[0].size)
    f3.fill_between(out[0],mean_gi-std_gi,mean_gi+std_gi,color='k',alpha=.3)
    f3.set_xlim(0,out[0].max())
    f3.set_ylabel('inhibition (uS)')
    plt.show()


def kuhn_comp(fe,fi):
    """
    takes fexc and finh,
    Param has to be set externally

    -> returns firing freq, approx kuhn formula
    """
    Gl=Param[0]
    Cm=Param[1]
    Te=Param[2]
    Ti=Param[3]
    Ee=Param[4]
    Ei=Param[5]
    refrac=Param[6]
    Vthre=Param[7]
    Vreset=Param[8]
    El=Param[9]
    Nai = Param[10]
    Connect = Param[11]
    balance = Param[12]

    fe = fe*(1-balance)*Nai*Connect
    fi = fi*balance*Nai*Connect

    mGe=fe*Qe*Te
    sGe=Qe*np.sqrt(fe*Te/2)
    mGi=fi*Qi*Ti
    sGi=Qi*np.sqrt(fi*Ti/2)

    Tm=Cm/(Gl+mGe+mGi)
    TeB=Te*Tm/(Te+Tm)
    TiB=Ti*Tm/(Ti+Tm)

    kl=2*Cm*Gl
    ke=2*Cm*mGe
    ki=2*Cm*mGi
    ue=TeB*sGe*sGe
    ui=TiB*sGi*sGi
    S0=kl+ke+ki+ue+ui
    S1=kl*El+ke*Ee+ki*Ei+ue*Ee+ui*Ei
    
    mVm=S1/S0
    sVm= np.sqrt( ( S0**2 * (ue*Ee**2+ui*Ei**2)\
                                - 2*S0*S1*(ue*Ee+ui*Ei) \
                                  + S1**2*(ue+ui) ) / S0**3 )

    proba = sp_spec.erfc((Vthre-mVm)/(np.sqrt(2)*sVm))
    T = 1.0/(0.5/Tm*proba)
    return 1/(refrac+T)
